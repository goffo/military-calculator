-- MySQL dump 10.16  Distrib 10.1.31-MariaDB, for Linux (aarch64)
--
-- Host: localhost    Database: military
-- ------------------------------------------------------
-- Server version	10.1.31-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Ability`
--

DROP TABLE IF EXISTS `Ability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ability` (
  `skill` int(11) NOT NULL,
  `user` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`skill`,`user`),
  KEY `user` (`user`),
  CONSTRAINT `Ability_ibfk_1` FOREIGN KEY (`skill`) REFERENCES `Skill` (`codS`),
  CONSTRAINT `Ability_ibfk_2` FOREIGN KEY (`user`) REFERENCES `User` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ability`
--

LOCK TABLES `Ability` WRITE;
/*!40000 ALTER TABLE `Ability` DISABLE KEYS */;
INSERT INTO `Ability` VALUES (1,'bamarin',4),(1,'user',1),(2,'bamarin',7),(2,'user',1),(3,'bamarin',2),(4,'bamarin',5),(5,'bamarin',90),(5,'user',80);
/*!40000 ALTER TABLE `Ability` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Association`
--

DROP TABLE IF EXISTS `Association`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Association` (
  `weapon` int(11) NOT NULL,
  `statistic` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`weapon`,`statistic`),
  KEY `statistic` (`statistic`),
  CONSTRAINT `Association_ibfk_1` FOREIGN KEY (`weapon`) REFERENCES `Weapon` (`codW`),
  CONSTRAINT `Association_ibfk_2` FOREIGN KEY (`statistic`) REFERENCES `Statistic` (`codS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Association`
--

LOCK TABLES `Association` WRITE;
/*!40000 ALTER TABLE `Association` DISABLE KEYS */;
INSERT INTO `Association` VALUES (1,1,35),(1,2,50),(1,3,70),(1,4,22),(1,5,240),(1,6,51),(2,1,38),(2,2,75),(2,3,70),(2,4,17),(2,5,240),(2,6,58),(3,1,38),(3,2,55),(3,3,75),(3,4,14),(3,5,240),(3,6,64),(4,1,32),(4,2,60),(4,3,75),(4,4,14),(4,5,240),(4,6,91),(5,1,63),(5,2,30),(5,3,15),(5,4,25),(5,5,230),(5,6,93),(16,1,30),(16,2,80),(16,3,82),(16,4,15),(16,5,220),(16,6,70),(19,1,28),(19,2,80),(19,3,80),(19,4,35),(19,5,220),(19,6,90);
/*!40000 ALTER TABLE `Association` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Category`
--

DROP TABLE IF EXISTS `Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Category` (
  `codC` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`codC`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Category`
--

LOCK TABLES `Category` WRITE;
/*!40000 ALTER TABLE `Category` DISABLE KEYS */;
INSERT INTO `Category` VALUES (1,'Pistol'),(2,'Shotgun'),(3,'Machinegun'),(4,'SMG'),(5,'Rifle'),(6,'Sniper');
/*!40000 ALTER TABLE `Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Component`
--

DROP TABLE IF EXISTS `Component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Component` (
  `codComp` int(11) NOT NULL AUTO_INCREMENT,
  `weapon` int(11) DEFAULT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`codComp`),
  KEY `weapon` (`weapon`),
  KEY `type` (`type`),
  CONSTRAINT `Component_ibfk_1` FOREIGN KEY (`weapon`) REFERENCES `Weapon` (`codW`),
  CONSTRAINT `Component_ibfk_2` FOREIGN KEY (`type`) REFERENCES `Type` (`codT`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Component`
--

LOCK TABLES `Component` WRITE;
/*!40000 ALTER TABLE `Component` DISABLE KEYS */;
INSERT INTO `Component` VALUES (1,17,'M4A1-Gastube',NULL,1),(2,17,'M4A1-Barrel',NULL,2),(3,17,'M4A1-GunGrip',NULL,3),(4,17,'M4A1-ForeGrip',NULL,4),(5,17,'M4A1-Receiver',NULL,5),(6,NULL,'Suppressor 5.56x45mm',NULL,11),(7,16,'FS-GasTube',NULL,1),(8,16,'FS-Barrel',NULL,2),(9,16,'FS-GunGrip',NULL,3),(10,16,'FS-ForeGrip',NULL,4),(11,16,'FS-Receiver',NULL,5),(12,19,'AUGgastube',NULL,1),(13,19,'AUGbarrel',NULL,2),(14,19,'AUGgungrip',NULL,3),(15,19,'AUGforegrip',NULL,4),(16,19,'AUGreceiver',NULL,5),(17,1,'P20-Barrel',NULL,2),(18,1,'P20-Gungrip',NULL,3),(19,1,'P20-Body',NULL,7),(20,1,'P20-Slide',NULL,8),(21,2,'2Beretta-Barrel',NULL,2),(22,2,'2Beretta-Gungrip',NULL,3),(23,2,'2Beretta-Body',NULL,7),(24,2,'2Beretta-Slide',NULL,8),(25,3,'P25-Barrel',NULL,2),(26,3,'P25-Gungrip',NULL,3),(27,3,'P25-Body',NULL,7),(28,3,'P25-Slide',NULL,8),(29,4,'57-Barrel',NULL,2),(30,4,'57-Gungrip',NULL,3),(31,4,'57-Body',NULL,7),(32,4,'57-Slide',NULL,8),(33,5,'Deagle-Barrel',NULL,2),(34,5,'Deagle-Gungrip',NULL,3),(35,5,'Deagle-Body',NULL,7),(36,5,'Deagle-Slide',NULL,8);
/*!40000 ALTER TABLE `Component` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Composition`
--

DROP TABLE IF EXISTS `Composition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Composition` (
  `type` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  PRIMARY KEY (`type`,`category`),
  KEY `category` (`category`),
  CONSTRAINT `Composition_ibfk_1` FOREIGN KEY (`type`) REFERENCES `Type` (`codT`),
  CONSTRAINT `Composition_ibfk_2` FOREIGN KEY (`category`) REFERENCES `Category` (`codC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Composition`
--

LOCK TABLES `Composition` WRITE;
/*!40000 ALTER TABLE `Composition` DISABLE KEYS */;
INSERT INTO `Composition` VALUES (1,5),(2,1),(2,2),(2,5),(2,6),(3,1),(3,2),(3,5),(3,6),(4,5),(5,5),(6,6),(7,1),(8,1),(9,2),(10,2);
/*!40000 ALTER TABLE `Composition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Ingredient`
--

DROP TABLE IF EXISTS `Ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ingredient` (
  `material` int(11) NOT NULL,
  `component` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`material`,`component`),
  KEY `component` (`component`),
  CONSTRAINT `Ingredient_ibfk_1` FOREIGN KEY (`material`) REFERENCES `Material` (`codM`),
  CONSTRAINT `Ingredient_ibfk_2` FOREIGN KEY (`component`) REFERENCES `Component` (`codComp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ingredient`
--

LOCK TABLES `Ingredient` WRITE;
/*!40000 ALTER TABLE `Ingredient` DISABLE KEYS */;
INSERT INTO `Ingredient` VALUES (1,1,2),(1,10,1),(1,11,1),(1,15,1),(1,16,1),(1,17,1),(1,19,1),(1,21,2),(1,24,2),(1,26,2),(1,28,3),(1,30,1),(1,32,1),(1,34,1),(1,35,1),(2,2,1),(2,4,2),(2,5,1),(2,17,1),(2,19,1),(2,21,2),(2,24,2),(2,26,2),(2,28,2),(2,30,2),(2,32,2),(2,34,1),(2,36,1),(3,1,1),(3,2,1),(3,6,1),(3,8,3),(3,13,3),(3,17,1),(3,19,2),(3,20,1),(3,21,2),(3,23,4),(3,25,2),(3,26,2),(3,28,4),(3,29,2),(3,30,1),(3,33,2),(3,34,1),(3,35,2),(3,36,1),(4,5,4),(4,6,2),(4,7,3),(4,8,2),(4,12,3),(4,18,1),(4,19,1),(4,22,2),(4,24,2),(4,27,2),(4,28,2),(4,31,3),(4,32,2),(4,35,1),(4,36,1),(5,2,1),(5,3,1),(5,4,1),(5,5,1),(5,8,1),(5,12,6),(5,19,1),(5,24,2),(5,28,2),(5,32,1),(5,36,1),(6,3,1),(6,18,2),(6,22,4),(6,27,4),(6,31,1),(6,35,2),(7,5,1),(7,6,1),(7,11,2),(7,20,1),(7,25,2),(7,29,2),(7,33,2),(7,36,1),(8,6,2),(8,7,1),(8,12,1),(9,11,2),(10,9,1),(10,32,2),(11,9,2),(11,14,2),(12,6,1),(15,7,1),(15,12,1),(16,8,1),(16,13,1),(17,12,2),(20,7,1),(20,12,1),(20,16,1),(23,10,1),(23,15,1),(26,14,1);
/*!40000 ALTER TABLE `Ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Inventory`
--

DROP TABLE IF EXISTS `Inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Inventory` (
  `owner` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `material` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`owner`,`material`),
  KEY `material` (`material`),
  CONSTRAINT `Inventory_ibfk_1` FOREIGN KEY (`owner`) REFERENCES `User` (`username`),
  CONSTRAINT `Inventory_ibfk_2` FOREIGN KEY (`material`) REFERENCES `Material` (`codM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Inventory`
--

LOCK TABLES `Inventory` WRITE;
/*!40000 ALTER TABLE `Inventory` DISABLE KEYS */;
INSERT INTO `Inventory` VALUES ('bamarin',1,100077),('bamarin',2,121),('bamarin',3,106),('bamarin',4,130),('bamarin',5,125),('bamarin',6,49),('bamarin',7,54),('bamarin',8,3),('bamarin',10,3),('bamarin',11,6),('bamarin',15,3),('bamarin',16,3),('bamarin',17,4),('bamarin',20,5),('bamarin',23,3),('bamarin',26,4),('Martina',1,4),('Martina',2,8),('Martina',3,4),('Martina',4,8),('Martina',5,8),('Martina',6,2),('Martina',7,2),('Martina',9,6),('Martina',13,7),('user',1,8),('user',2,8),('user',3,12),('user',4,8),('user',5,4),('user',6,8),('user',7,0),('user',10,0);
/*!40000 ALTER TABLE `Inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Material`
--

DROP TABLE IF EXISTS `Material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Material` (
  `codM` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codM`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Material`
--

LOCK TABLES `Material` WRITE;
/*!40000 ALTER TABLE `Material` DISABLE KEYS */;
INSERT INTO `Material` VALUES (1,'Chunk of Plastic',NULL,200,'~/Pictures/military/chunk_plastic.png'),(2,'Metal Polish',NULL,50,NULL),(3,'Metal Rod',NULL,500,NULL),(4,'Piece of Metal',NULL,400,NULL),(5,'Paint Bucket',NULL,60,NULL),(6,'Hunk of Polymere',NULL,100,NULL),(7,'Box of springs',NULL,50,NULL),(8,'Wooden Board',NULL,400,NULL),(9,'Brass Bulk',NULL,100,NULL),(10,'Glass Shard',NULL,550,NULL),(11,'Cardboad Box',NULL,150,NULL),(12,'Cinderblock',NULL,500,NULL),(13,'Gun Powder',NULL,100,NULL),(14,'Propane Tank',NULL,1500,NULL),(15,'Iodine',NULL,200,NULL),(16,'Glue',NULL,125,NULL),(17,'Wooden Nails',NULL,100,NULL),(18,'Electronics',NULL,100,NULL),(19,'Paper Towel',NULL,130,NULL),(20,'Empty Bottle',NULL,100,NULL),(21,'Salt',NULL,100,NULL),(22,'Kitty',NULL,200,NULL),(23,'100% Refined Metal',NULL,4800,NULL),(24,'75% Refined Metal',NULL,2700,NULL),(25,'50% Refined Metal',NULL,1600,NULL),(26,'25% Refined Metal',NULL,600,NULL);
/*!40000 ALTER TABLE `Material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Optional`
--

DROP TABLE IF EXISTS `Optional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Optional` (
  `realization` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `component` int(11) NOT NULL,
  PRIMARY KEY (`realization`,`type`),
  KEY `type` (`type`),
  KEY `component` (`component`),
  CONSTRAINT `Optional_ibfk_1` FOREIGN KEY (`realization`) REFERENCES `Realization` (`codR`),
  CONSTRAINT `Optional_ibfk_2` FOREIGN KEY (`type`) REFERENCES `Type` (`codT`),
  CONSTRAINT `Optional_ibfk_3` FOREIGN KEY (`component`) REFERENCES `Component` (`codComp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Optional`
--

LOCK TABLES `Optional` WRITE;
/*!40000 ALTER TABLE `Optional` DISABLE KEYS */;
INSERT INTO `Optional` VALUES (1,11,6);
/*!40000 ALTER TABLE `Optional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Realization`
--

DROP TABLE IF EXISTS `Realization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Realization` (
  `codR` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weapon` int(11) NOT NULL,
  PRIMARY KEY (`codR`),
  KEY `user` (`user`),
  KEY `weapon` (`weapon`),
  CONSTRAINT `Realization_ibfk_1` FOREIGN KEY (`user`) REFERENCES `User` (`username`),
  CONSTRAINT `Realization_ibfk_2` FOREIGN KEY (`weapon`) REFERENCES `Weapon` (`codW`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Realization`
--

LOCK TABLES `Realization` WRITE;
/*!40000 ALTER TABLE `Realization` DISABLE KEYS */;
INSERT INTO `Realization` VALUES (1,'bamarin',17),(2,'bamarin',19),(3,'bamarin',3),(4,'user',4);
/*!40000 ALTER TABLE `Realization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RequirementC`
--

DROP TABLE IF EXISTS `RequirementC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RequirementC` (
  `skill` int(11) NOT NULL,
  `component` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`skill`,`component`),
  KEY `component` (`component`),
  CONSTRAINT `RequirementC_ibfk_1` FOREIGN KEY (`skill`) REFERENCES `Skill` (`codS`),
  CONSTRAINT `RequirementC_ibfk_2` FOREIGN KEY (`component`) REFERENCES `Component` (`codComp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RequirementC`
--

LOCK TABLES `RequirementC` WRITE;
/*!40000 ALTER TABLE `RequirementC` DISABLE KEYS */;
INSERT INTO `RequirementC` VALUES (1,1,1),(1,2,1),(1,3,1),(1,4,1),(1,5,1),(1,6,1),(1,7,1),(1,8,1),(1,9,1),(1,10,1),(1,11,1),(1,12,1),(1,13,1),(1,14,1),(1,15,1),(1,17,1),(1,18,1),(1,19,1),(1,20,1),(1,21,1),(1,22,1),(1,23,1),(1,24,1),(1,25,1),(1,26,1),(1,27,1),(1,28,1),(1,29,1),(1,30,1),(1,31,1),(1,32,1),(1,33,1),(1,34,1),(1,35,1),(1,36,1),(2,1,1),(2,2,1),(2,3,1),(2,4,1),(2,5,1),(2,6,1),(2,7,1),(2,8,1),(2,9,1),(2,10,1),(2,11,1),(2,12,1),(2,13,1),(2,14,1),(2,15,1),(2,16,1),(2,17,1),(2,18,1),(2,19,1),(2,20,1),(2,21,1),(2,22,1),(2,23,1),(2,24,1),(2,25,1),(2,26,1),(2,27,1),(2,28,1),(2,29,1),(2,30,1),(2,31,1),(2,32,1),(2,33,1),(2,34,1),(2,35,1),(2,36,1),(5,1,80),(5,2,81),(5,3,85),(5,4,80),(5,5,87),(5,6,91),(5,7,75),(5,8,76),(5,9,77),(5,10,78),(5,11,80),(5,12,86),(5,13,87),(5,14,88),(5,15,89),(5,16,90),(5,17,3),(5,18,3),(5,19,5),(5,20,7),(5,21,15),(5,22,17),(5,23,19),(5,24,20),(5,25,25),(5,26,27),(5,27,29),(5,28,30),(5,29,31),(5,30,33),(5,31,34),(5,32,35),(5,33,37),(5,34,38),(5,35,39),(5,36,40);
/*!40000 ALTER TABLE `RequirementC` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RequirementW`
--

DROP TABLE IF EXISTS `RequirementW`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RequirementW` (
  `skill` int(11) NOT NULL,
  `weapon` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`skill`,`weapon`),
  KEY `weapon` (`weapon`),
  CONSTRAINT `RequirementW_ibfk_1` FOREIGN KEY (`skill`) REFERENCES `Skill` (`codS`),
  CONSTRAINT `RequirementW_ibfk_2` FOREIGN KEY (`weapon`) REFERENCES `Weapon` (`codW`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RequirementW`
--

LOCK TABLES `RequirementW` WRITE;
/*!40000 ALTER TABLE `RequirementW` DISABLE KEYS */;
INSERT INTO `RequirementW` VALUES (1,1,1),(1,2,1),(1,3,1),(1,4,1),(1,5,1),(1,16,1),(1,17,1),(1,19,1),(2,1,1),(2,2,1),(2,3,1),(2,4,1),(2,5,1),(2,16,1),(2,17,1),(2,19,1),(5,1,10),(5,2,20),(5,3,30),(5,4,35),(5,5,40),(5,16,80),(5,17,87),(5,19,90);
/*!40000 ALTER TABLE `RequirementW` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Skill`
--

DROP TABLE IF EXISTS `Skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Skill` (
  `codS` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`codS`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Skill`
--

LOCK TABLES `Skill` WRITE;
/*!40000 ALTER TABLE `Skill` DISABLE KEYS */;
INSERT INTO `Skill` VALUES (1,'Intelligence'),(2,'Strength'),(3,'Wisdom'),(4,'Dexterity'),(5,'Crafting');
/*!40000 ALTER TABLE `Skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Statistic`
--

DROP TABLE IF EXISTS `Statistic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Statistic` (
  `codS` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codS`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Statistic`
--

LOCK TABLES `Statistic` WRITE;
/*!40000 ALTER TABLE `Statistic` DISABLE KEYS */;
INSERT INTO `Statistic` VALUES (1,'Damage'),(2,'Firerate'),(3,'Recoil control'),(4,'Accurate range'),(5,'Mobility'),(6,'Armor penetration');
/*!40000 ALTER TABLE `Statistic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Type`
--

DROP TABLE IF EXISTS `Type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Type` (
  `codT` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codT`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Type`
--

LOCK TABLES `Type` WRITE;
/*!40000 ALTER TABLE `Type` DISABLE KEYS */;
INSERT INTO `Type` VALUES (1,'Gas tube'),(2,'Barrel'),(3,'Gungrip'),(4,'Foregrip'),(5,'Receiver'),(6,'Muzzle brake'),(7,'Body'),(8,'Slide'),(9,'Pump'),(10,'Stock'),(11,'Suppressor'),(12,'Sight');
/*!40000 ALTER TABLE `Type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civic` int(11) DEFAULT NULL,
  `city` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES ('bamarin','paolo21','paolo@marini.com','Leonardo','Marini','via dei Mille',45,'Forlimpopoli','Italy',47034),('Martina','ciaociao','martina.cavallucci@ciao.it','Martina','Cavallucci','ciaociao',55,'forli','italia',2235),('nic0las','nicolas','nicolas.farabegoli@nicolas.com','Nicolas','Farabegoli','Cento',123,'Bertinoro','Italy',78695),('user','user','simple.user@user.com','John','Doe',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Weapon`
--

DROP TABLE IF EXISTS `Weapon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Weapon` (
  `codW` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`codW`),
  KEY `category` (`category`),
  CONSTRAINT `Weapon_ibfk_1` FOREIGN KEY (`category`) REFERENCES `Category` (`codC`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Weapon`
--

LOCK TABLES `Weapon` WRITE;
/*!40000 ALTER TABLE `Weapon` DISABLE KEYS */;
INSERT INTO `Weapon` VALUES (1,'P2000',1,NULL),(2,'Dual Berettas',1,NULL),(3,'P250',1,NULL),(4,'Five-SeveN',1,NULL),(5,'Desert Eagle',1,NULL),(6,'Nova',2,NULL),(7,'XM1014',2,NULL),(8,'Mag-7',2,NULL),(9,'M249',3,NULL),(10,'Negev',3,NULL),(11,'MP9',4,NULL),(12,'MP7',4,NULL),(13,'UMP-45',4,NULL),(14,'P90',4,NULL),(15,'PP-Bizon',4,NULL),(16,'FAMAS',5,NULL),(17,'M4A1',5,NULL),(18,'SSG 08',6,NULL),(19,'AUG',5,NULL),(20,'AWP',6,NULL),(21,'SCAR-20',6,NULL);
/*!40000 ALTER TABLE `Weapon` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-10 21:35:56
