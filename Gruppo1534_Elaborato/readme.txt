readme.txt

:Author: Nicolas Farabegoli
:Email: nicolas.farabegoli@gmail.com
:Date: 2018-07-10 21:41

Dal momento che il file .jar ha dimensioni superiori a 10M non è stato possibile
caricarlo sul portale, è stato quindi caricato sulla sezione Download rella repo
BitBucket del progetto; quindi andare al seguente link e scaricare il file
military-calculator-0.0.1.jar

https://bitbucket.org/goffo/military-calculator/downloads/

Una volta scaricato il file military-calculator-0.0.1.jar, eseguirlo mediante il
comando: java -jar military-calculator-0.0.1.jar.
Nota: l'applicativo è stato sviluppato con JDK 8, i test effettuati sono stati
effettuati con JDK 9, si consiglia quest'ultima per l'esecuzione anche se non ci
dovrebbero essere problemi con Java 8.

Una volta avviata l'applicazione, questa si collegherà in automatico al DB
(hostato in un server remoto), ci si assicuri che eventuali firewall non
blocchino la connessione verso il server remoto, in tal caso l'applicazione
genererà un eccezione indicando che non è riuscita ad instaurare una
comunicazione con il server remoto.

Avviata l'applicazione si presenta una form di login, le credenziali per poter
effettuare i test sono le seguenti:
Username: user
Password: user

A questo punto l'applicazione mostra la schermata principale. nelle tab in alto
a sinistra sono elencate le categorie di armi, cliccando vengono proposte tutte
le armi appartenenti a quella categoria, selezionando l'arma appare un pop-up
che visualizza tutti i componenti che sono necessari alla costruzione, oltre a
statistiche e abilità relative all'arma. Se nel proprio inventario sono presenti
sufficienti materiali alla costruzioni è possibile, mediante il pulsante craft,
costruire immediatamente l'arma. Nel caso in cui non si disponga di sufficienti
materiali, mediante il pulsante "Add materials", è possibile aggiungere al
carrello i materiali necessari alla costruzione dell'arma. In seguito è
possibile integrare al carrello ulteriori materiali attraverso la parte
centrale della gui. Selezionando il materiale verrà chiesto il quantitativo
desiderato, con Ok verrà aggiunto al carrello.

In basso a sinistra, sotto al carrello, con il pulanste Buy, si possono
acquistare i materiali presenti nel carrello. A seguito dell'acquisto
l'inventario presente nella parte a sinistra, si aggiornerà conseguentemente.

Allo stato attuale è possibile che alcune categorie di armi non siano poplate a
livello di DB, consigliamo quindi di testare le seguenti categorie in quanto
solo queste sono state popolate correttamente: Riffle, Pistol.

Infine è possibile testare la funzionalità di registrazione di un nuovo utente:
nella schermata principale cliccare sul pulsante "Register", comparirà una form
da compilare, a seguito della compilazione della form cliccare su "Submit": un
pop-up avvisa dell'avvenuta registrazione del nuovo utente. A questo punto
cliccare su "Back" per tornare alla schermata principale ed effettuare il login
con le credenziali appena inserite per il nuovo utente.

Connessione remota al DB:
URL: 88.147.17.142:3306
Username: user
Password: pswd

Con queste credenziali si ha accesso COMPLETO al database con TUTTI i privilegi.
