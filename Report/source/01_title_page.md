\begin{titlepage}
    \centering
    \vspace*{0.5 cm}
    \textsc{\LARGE Università degli Studi di Bologna}\\[2.0 cm]  % University Name
    \textsc{\Large Basi di Dati}\\[0.5 cm]               % Course Code

    \vspace*{2 cm}

    \rule{\linewidth}{0.2 mm} \\[0.4 cm]
    { \huge \bfseries Military calculator }\\
    \rule{\linewidth}{0.2 mm} \\[1.5 cm]

    \vspace*{5 cm}

    \begin{minipage}{0.4\textwidth}
    \end{minipage}~
    \begin{minipage}{0.4\textwidth}
        \begin{center} \large
            \emph{Realizzata da:} \\
            \vspace*{2mm}
            Leonardo Marini \\
            Nicolas Farabegoli\\
        \end{center}
    \end{minipage}\\[2 cm]
\end{titlepage}

