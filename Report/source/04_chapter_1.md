﻿# Analisi dei requisiti

## Intervista

In seguito alla commissione da parte di un cliente, coinvolto nella produzione
di videogiochi a tema di guerra, è stata condotta un'intervista, la quale ha
rivelato che l'interessato vuole sviluppare un'applicazione dedita ad
accompagnare l'utente durante tutto il processo di produzione di un'arma da				<!--arma - arma da fuoco-->
fuoco: dall'acquisto dei materiali necessari, fino all'assemblaggio dei suoi
componenti.

Durante l'intervista sono state poste domande per capire quali fossero le
funzionalità principali richieste dal cliente. L'obiettivo dell'intervista è
anche quello di verificare la necessità dell'utilizzo di un database per gestire
le informazioni di back-end.

Lo scopo principale del sistema che si vuole sviluppare è quello di calcolare la
spesa per l'acquisto di materiali per la produzione di armi. Inoltre, per
facilitare ulteriormente gli utenti, l'applicazione deve essere in grado di elencare		<!--[MATERIAL]-->
i vari materiali necessari per la costruzione di una specifica arma.				<!--[WEAPON]-->

Andando più nel dettaglio si è capito che le armi possono essere classificate			<!--[CATEGORIE]-->
secondo vari parametri, quali la lunghezza, la ripetizione dello sparo, il tipo di
impiego e il suo funzionamento.

Da quest'ultima categorizzazione dipendono i componenti presenti nell'arma.				<!--[OPTIONAL] - chiarimento-->
Tuttavia, alcuni componenti possono essere montati a descrizione dell'utente,
indipendentemente dal tipo di arma.

Per ciò che riguarda gli utenti, il cliente ha specificato che nei videogiochi			<!--[USER]-->
da lui già prodotti ogni utente ha un suo inventario, un portafoglio e un				<!--- quantità di denaro-->
insieme di abilità nelle quali può aumentare di livello. Dunque affinché la
nuova applicazione si possa interfacciare ai precedenti videogiochi, è
necessario gestire gli utenti, che dovranno eseguire un login per poter
utilizzare le funzionalità offerte.

La perplessità sulla necessità di avere salvate le abilità degli utenti e su			<!--[SKILL]-->
quale fosse la sua utilità nell'applicazione è stata superata in
quanto si ha appreso che le armi hanno diversi gradi di difficoltà per poter essere realizzate.
Un utente è in grado di costruire un determinato fucile solo se le sue abilità
sono pari o migliori rispetto a quelle richieste dall'arma.

A tal proposito, si rende necessario l'utilizzo di un database per memorizzare
le armi che è possibile costruire, oltre ai materiali che ogni arma necessita
per essere costruita. Inoltre, gli utenti e le loro relazioni con armi e
componenti aggiungono un notevole grado di complessità alla strutturazione delle
informazioni, comunque facilmente gestibili attraverso l'uso di un database
relazionale.

## Rilevamento delle ambiguità e correzioni proposte

### Generalizzazione di arma
È importante chiarire che i termini "arma" e "arma da fuoco" sono da intendersi
equivalenti, in quanto il database che verrà sviluppato tratta esclusivamente
armi da fuoco. Allo stesso modo "pistola" e "fucile" sono anch'esse armi da
fuoco, appartenenti a diverse categorie, ma nel contesto nel quale vengono
usate, il termine può essere sostituito con "arma da fuoco" (di qualsiasi
categoria).

### Categorizzazione utile
Tra i vari tipi di categorizzazione che possono essere usati per individuare un
gruppo di armi, l'unico rilevante ai fini dell'ideazione del database è quello
che le discrimina in base al loro funzionamento. Le restanti non verranno prese
in considerazione in quanto non hanno influenza nel processo di produzione.

### Componenti optional
Data una categoria è possibile sapere che tipo di componenti richiede un'arma da
fuoco. Tuttavia questo è vero solo per componenti base. Altri, che "possono
essere montati a descrizione dell'utente", verranno successivamente chiamati
*"optionals"*.

### Portafoglio
Il termine "portafoglio" è da intendersi semplicemente come quantità di denaro
accessibile da un utente. È utile per sapere quali materiali, un determinato
utente, è in grado di comprare.

### Realizzazione di un'arma
Le armi che è possibile costruire sono prestabilite, gli utenti potranno
consultarle mediante una lista

\newpage

## Definizione delle specifiche in linguaggio naturale ed estrazione dei concetti principali

Segue una descrizione del progetto che si intende realizzare, sintetizzata e
ripulita da termini ambigui, rispetto all'intervista di cui sopra. Sono stati
evidenziati i termini che verranno successivamete tradotti in entità.

Si vuole realizzare un database che gestisca la produzione di armi da fuoco,
prodotte da video-giocatori per giochi di guerra.

Le **armi** sono caraterizzate dalla **categoria** alla quale appartengono
(Rifle, Shotgun, Pistol, ...), un nome, un'immagine esempio, un codice
identificativo del modello e un insieme di **statistiche**. Inoltre,
ciascun'arma, è scomponibile in più **componenti**, i quali sono caratterizzati
da un nome, un codice identificativo e un **tipo**, che indica il ruolo svolto
da quel componente. La categoria dell'arma serve a definire quali sono i suoi
componenti, o per meglio dire i loro tipi. Siccome un'arma è un insieme di
componenti, questo implica che non può avere due componenti dello
stesso tipo.

Alcuni componenti, detti **optionals**, non dipendono dalla categoria e possono
essere presenti o no in qualsiasi arma, purché ce ne sia al massimo uno di ogni
tipo. Infine ogni componente è composto da un'insieme di **materiali** in
quantità variabili.

Questi  ultimi sono caratterizzati da un nome, un prezzo, una descrizione e
un'immagine d'esempio. Vengono utilizzati nella costruzione di componenti e 
possono essere acquistati da un **utente** che abbia sufficiente denaro nel
proprio portafoglio.

Infine gli utenti, attraverso i materiali posseduti nel proprio inventario,
possono realizzare armi da fuoco, specificando la presenza di eventuali
optionals. Un utente, per poter realizzare un'arma, deve essere sufficientemente
competente nelle **abilità** e possedere tutti i materiali necessari alla sua
costruzione.
