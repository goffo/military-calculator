# Progettazione concettuale

## Schema scheletro

Una volta completata la fase di analisi, si è passati alla progettazione del
database. Partendo dai concetti fondamentali, ricavati dall'analisi, si è
proceduto realizzando dei modelli concettuali *Entity-Relationship* indipendenti
tra loro.

### Armi

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.4]{source/screen/ERW.png}
    \caption{Schema E/R delle armi}
\end{figure}

Il primo passo è stato quello di rappresentare le armi come un assemblaggio di
più componenti. Per quanto riguarda le caratteristiche delle armi sono state
tutte tradotte in attributi, eccetto le statistiche e la categoria. Infatti, le
statistiche sono state tradotte in un'entità, poiché avere un'attributo per ogni
statistica sarebbe stato scomodo e inefficiente. Allo stesso modo anche
"categoria" è stata tradotta in entità, in quanto deve anche essere possibile
accedere alle armi tramite una categoria, ciò significa che la categoria non
dipende da una singola arma. Infatti più armi possono appartenere alla stessa
categoria.

### Componenti

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.4]{source/screen/ERC.png}
    \caption{Schema E/R dei componenti}
\end{figure}

Per quanto riguarda i componenti, essi sono composti da un'insieme di materiali
in varie quantità. Chiaramente, lo stesso materiale può essere usato per
costruire più componenti. Ogni componente ha un "tipo" che definisce il suo
ruolo (Calcio, Canna, Castello, Impugnatura...). Una volta che un gruppo di
componenti vengono assemblati in un'arma, questi non possono essere più
disassemblati e riassemblati in un'altra arma. Ecco perché, secondo questo
schema, un componente può essere assemblato in una sola arma. Infine ogni
componente si distingue in componente base o componente optional. Rendendo
l'entità componente una generalizzazione totale esclusiva.

\newpage

### Utenti

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.5]{source/screen/ERU.png}
    \caption{Schema E/R degli utenti}
\end{figure}

L'ultimo macro-schema è quello riguardante gli utenti. Come da specifiche ogni
utente possiede i materiali che gli permettono di costruire le armi. È anche
competente in un numero di attività che gli permettono di assemblare armi sempre
più complesse. Ogni utente è identificato tramite il suo username che viene
inserito nel database durante la registrazione, insieme a tutti gli altri dati
personali, eccetto l'indirizzo che può essere inserito a discrezione dell'utente
per motivi di privacy.

\newpage

## Raffinamenti proposti

Il seguente schema rappresenta l'unione dei macro schemi prodotti finora.
Tuttavia sono ancora necessari dei miglioramenti, che è possibile aggiungere ora
che abbiamo definito tutte le entità e come sono in relazione tra loro.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.4]{source/screen/ERWCU.png}
    \caption{Schema E/R contenente tutte le entità}
    \label{ERWCU}
\end{figure}

Come specificato in fase di analisi, la categoria definisce i tipi di componenti
di un'arma: quindi *CATEGORIA* dovrebbe essere in relazione a *TIPO*, questo però è
vero solo per i componenti base. Ci siamo dunque accorti che la suddivisione
*BASE / OPTIONAL* non riguarda direttamente i componenti, ma piuttosto i tipi.
Si è quindi spostata la generalizzazione sull'entità tipo, e si è collegata
la categoria alle due specializzazioni di tipo Figura \ref{ERWCU}.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.5]{source/screen/ERCat.png}
    \caption{Correzione generalizzazione}
    \label{ERCat}
\end{figure}

\newpage

Un altro aspetto che è stato finora trascurato è quello delle *ABILITÀ*. La
corrispondente entità è già stata, in partenza, collegata ad *UTENTE*. Si rende
però necessario anche collegarla alle entità *ARMA* e *COMPONENTE*, in quanto
l'abilità serve a determinare se un utente è in grado di costruire componenti ed
assemblarli in armi. Il risultato parziale, del raffinamento proposto è quello
mostrato nella seguente figura (Figura \ref{ERSkill}).

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.5]{source/screen/ERSkill.png}
    \caption{Raffinamento del concetto di Abilità}
    \label{ERSkill}
\end{figure}

\newpage

## Schema concetuale finale

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{source/screen/ER4.png}
    \caption{Schema E/R finale}
    \label{ER}
\end{figure}

