# Progettazione logica
## Stima del volume dei dati

\begin{table}[H]
  \centering
  \begin{tabular}{|l|c|r|}
    \hline
    \textbf{Concetto} & \textbf{Costrutto} & \textbf{Volume} \tabularnewline
    \hline \hline
    Richiede-C  &    R    &   480 \tabularnewline \hline
    Abilità     &    E    &     5 \tabularnewline \hline
    Richiede-A  &    R    &   105 \tabularnewline \hline
    Competeza   &    R    &   400 \tabularnewline \hline
    Utente      &    E    &   100 \tabularnewline \hline
    Inventario  &    R    &   350 \tabularnewline \hline
    Materiale   &    E    &    55 \tabularnewline \hline
    Composizione&    R    &   640 \tabularnewline \hline
    Componente  &    E    &   160 \tabularnewline \hline
    Assemblaggio&    R    &   160 \tabularnewline \hline
    Ruolo       &    R    &   160 \tabularnewline \hline
    Tipo        &    E    &    12 \tabularnewline \hline
    Base        &    E    &    10 \tabularnewline \hline
    Optional    &    E    &     2 \tabularnewline \hline
    Presenza-B  &    R    &    24 \tabularnewline \hline
    Presenza-O  &    R    &     2 \tabularnewline \hline
    Categoria   &    E    &     6 \tabularnewline \hline
    Arma        &    E    &    35 \tabularnewline \hline
    Possiede    &    R    &   220 \tabularnewline \hline
    Statistica  &    E    &     6 \tabularnewline \hline
    Appartenenza&    R    &    35 \tabularnewline \hline
  \end{tabular}
  \caption{Tabella dei volumi}
\end{table}


## Descrizione delle operazioni principali e stima della loro frequenza

|Cod|Operazione                                                         |Tipo|Frequenza	|
|--:|:------------------------------------------------------------------|:--:|---------:|
|  1|Lettura dell'inventario di un utente                               | B  |150/gg   	|
|  2|Acquisto di un materiale da parte di un utente                     | I  |400/gg   	|
|  3|Visualizzazione di tutti i materiali                               | B  |50/gg    	|
|  4|Autenticazione di un utente                                        | I  |50/gg    	|
|  5|Registrazione di un nuovo utente nel DB                            | I  |1/mese   	|
|  6|Lettura di tutte le categorie                                      | B  |50/gg    	|
|  7|Visualizzazione armi filtrate per categoria                        | B  |100/gg   	|
|  8|Armi possedute da uno specifico utente                             | B  |150/gg   	|
|  9|Visualizzazione dei materiali necessari alla costruzione di un'arma| I  |100/gg   	|
| 10|Visualizzazione requisiti arma                                     | I  |100/gg   	|
| 11|Visualizzazione statistiche arma                                   | I  |100/gg   	|

### Spiegazione dei valori di frequenza

Si suppone che dati tutti gli utenti (100), l'applicazione venga utilizzata solo
dalla metà di essi in un giorno. (50)

Si suppongono 100 aquisti al giorno (2 per utente)

Si suppone che, essendo un'app per videogiochi già in produzione, le nuove
registrazioni saranno relativamente basse

1. Tutte le volte che viene effettuato un acquisito (2) + 1 quando si accede
   all'applicazione (3 * 50).
2. Numero di materiali nel carrello, ovvero numero di materiali richiesti per
   un'arma (160 / 35 = 4), per il numero di acquisti al giorno (100).
3. 1 per accesso all'applicazione.
4. 1 per accesso all'applicazione.
5. 1 ogni nuova registrazione.
6. 1 per accesso all'applicazione.
7. 1 per acquisto.
8. Come per l'inventario (vedi punto 1).
9. 1 per acquisto.
10. 1 per acquisto.
11. 1 per acquisto.

\newpage

## Schemi di navigazione e tabelle degli accessi

Di seguito sono riportate le tabelle degli accessi di alcune delle operazioni
sopra citate. Le restanti sono state tralasciate in quanto il calcolo del costo
è simile a quelle già elencate, o perchè sono talmente poco dispendiose da rendere
il loro costo trascurabile.

### Lettura dell'inventario di un utente

Si vuole visualizzare l'elenco di tutti i materiali che un utente possiede.

\begin{figure}[H]
  \begin{minipage}{0.48\textwidth}
    \centering
    \includegraphics[scale=0.6]{source/screen/schema-nav-lettura-inventario.png}
  \end{minipage}\hfill
\end{figure}

: Accessi lettua inventario utente

|  Concetto  | Costrutto | Accessi    | Tipo (Lettura/Scrittura) |
|:----------:|:---------:|:----------:|:------------------------:|
|   Utente   |     E     |     1      |             L            |
| Inventario |     R     |(350/100)=35|             L            |
|  Materiale |     E     |    35      |             L            |

**Totale**: 71L

**Frequenza**: 150/gg

**Costo totale**: 71 x 150 = 10650/gg
\newpage

### Acquisto di un materiale da parte di un utente

Si vuole inserire nell'inventario il materiale acquistato da un determinato
utente.

\begin{figure}[H]
  \begin{minipage}{0.48\textwidth}
    \centering
    \includegraphics[scale=0.6]{source/screen/schema-nav-acquisto-mat.png}
  \end{minipage}\hfill
\end{figure}

: Accessi acquisto materiale

|  Concetto  | Costrutto | Accessi | Tipo (Lettura/Scrittura) |
|:----------:|:---------:|:-------:|:------------------------:|
|  Materiale |     E     |    1    |             L            |
|   Utente   |     E     |    1    |             L            |
| Inventario |     R     |    1    |             L            |
| Inventario |     R     |    1    |             S            |

**Totale**: 3L+1S = 5

**Frequenza**: 400/gg

**Costo totale**: 5 x 400 = 2000/gg
\newpage

### Visualizzazione dei materiali necessari alla costruzione di un'arma

Si vuole visualizzare l'elenco di tutti i materiali che sono necessari alla
realizzazione di un arma.

\begin{figure}[H]
  \begin{minipage}{0.48\textwidth}
    \centering
    \includegraphics[scale=0.6]{source/screen/schema-nav-materiali-per-armi.png}
  \end{minipage}\hfill
\end{figure}

: Accessi visualizzazzione materiali per costruzione arma

|    Concetto   | Costrutto | Accessi    | Tipo (Lettura/Scrittura) |
|:-------------:|:---------:|:----------:|:------------------------:|
|      Arma     |     E     |     1      |             L            |
| Assemblaggio  |     R     |(160/35)=4  |             L            |
|   Componente  |     E     |    4       |             L            |
|  Composizione |     R     |640/160x4=16|             L            |
|   Materiale   |     E     |    16      |             L            |

**Totale**: 41L

**Frequenza**: 100/gg

**Costo totale**: 41 * 100 = 4100/gg
\newpage

### Armi possedute da uno specifico utente

Si vogliono visulizzare tutte le armi possedute da un utente.

\begin{figure}[H]
  \begin{minipage}{0.48\textwidth}
    \centering
    \includegraphics[scale=0.6]{source/screen/schema-nav-armi-possedute.png}
  \end{minipage}\hfill
\end{figure}

: Accessi armi possedute da un utente

|   Concetto   | Costrutto | Accessi | Tipo (Lettura/Scrittura) |
|:------------:|:---------:|:-------:|:------------------------:|
|    Utente    |     E     |   1     |             L            |
| Assemblaggio |     R     |160/35=4 |             L            |
|     Arma     |     E     |     4   |             L            |

**Totale**: 9L

**Frequena**: 150/gg

**Costo totale**: 9 * 150 = 1350/gg
\newpage

### Visulizzazione statistiche arma

Si vogliono visualizzare le statistiche relative ad un arma.

\begin{figure}[H]
  \begin{minipage}{0.44\textwidth}
    \centering
    \includegraphics[scale=0.6]{source/screen/schema-nav-statistiche.png}
  \end{minipage}\hfill
\end{figure}

: Accessi statistiche arma

|  Concetto  | Costrutto | Accessi | Tipo (Lettura/Scrittura) |
|:----------:|:---------:|:-------:|:------------------------:|
|    Arma    |     E     |     1   |             L            |
|  Possiede  |     R     |220/35=6 |             L            |
| Statistica |     E     |    6    |             L            |

**Totale**: 13L

**Frequenza**: 100/gg

**Costo totale**: 13 * 100 = 1300/gg

### Aggiornamento inventario a seguito di costruzione arma

Si vuole decrementare la quanità di materiali appartenenti ad un utente.

\begin{figure}[H]
  \begin{minipage}{0.44\textwidth}
    \centering
    \includegraphics[scale=0.6]{source/screen/schema-nav-component-update.png}
  \end{minipage}\hfill
\end{figure}

: Aggiornamento inventario materiali

| Concetto     | Costrutto   | Accessi   | Tipo (Lettura/Scrittura)   |
| :----------: | :---------: | :-------: | :------------------------: |
| Arma         | E           | 1         | L                          |
| Assemblaggio | R           | 4         | L                          |
| Componente   | E           | 4         | L                          |
| Composizione | R           | 16        | L                          |
| Materiale    | E           | 16        | L                          |
| Inventario   | R           | 16        | L                          |
| Inventario   | R           | 16        | S                          |
| Utente       | E           | 1         | L                          |

**Totale**: 74L + 16S = 90

**Frequena**: 100/gg

**Costo totale**: 90 * 100 = 9000/gg

### Inserimento nuova arma per utente

Si vuole aggiungere una nuova arma ad uno specifico utente.

\begin{figure}[H]
  \begin{minipage}{0.44\textwidth}
    \centering
    \includegraphics[scale=0.6]{source/screen/schema-nav-add-weapon.png}
  \end{minipage}\hfill
\end{figure}

: Inserimento arma utente

| Concetto     | Costrutto   | Accessi   | Tipo (Lettura/Scrittura)   |
| :----------: | :---------: | :-------: | :------------------------: |
| Arma         | E           | 1         | L                          |
| Assemblaggio | R           | 1         | S                          |
| Utente       | E           | 1         | L                          |

**Totale**: 2L + 1S = 4

**Frequena**: 100/gg

**Costo totale**: 100 * 4 = 400/gg


\newpage

## Raffinamento dello schema <!--(eliminazione di identificatori esterni, attributi composti e gerarchie, scelta delle chiavi)-->
### Eliminazione di identificatori esterni
Successivamente alla definizione dello schema relazionale rappresentato in
*Figura \ref{ER}* è necessario apportare qualche raffinamento, il primo dei quali
riguarda la scelta dell'identificatore di componente. Quest'ultimo è infatti
univocamente identificabile attraverso l'attributo *codC* ma anche dall'insieme
degli identificatori di *ARMA* e *TIPO*. Si decide dunque ti mantenere
l'attributo *codC* e rimuovere l'identificatore esterno in quanto non
necessario.

### Scomposizione di attributi composti
Lo schema E/R finale *(Figura \ref{ER})* contiene un attributo composto opzionale,
riguardante alcune informazioni personali dell'utente. La composizione di questo
attributo è stata risolta con la semplice scomposizione dei sotto attributi di
*indirizzo* in singoli attributi collegati all'entità *UTENTE*. Per quanto
riguarda l'opzionalità dell'attributo si è deciso che in fase di registrazione
l'utente può lasciare i suddeti campi vuoti. In tal caso le rispettive celle
verranno settate a *NULL*.

### Accorpamento delle gerarchie
L'unica gerarchia presente nello schema E/R finale *(Figura \ref{ER})* è quella
sull'entità *TIPO*. In questo caso un accorpamento verso il basso risulterebbe
nell'avere due relazioni separate che descrivono lo stesso concetto: una che
collega *COMPONENTE* a *BASE*, e l'altra che collega *COMPONENTE* a *OPTIONAL*.
È stato quindi scelto un accorpamento verso l'alto, scelta resa più conveniente
anche dal fatto che le entità figlie non hanno attibuti che le distinguono.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.6]{source/screen/ERT.png}
    \caption{Risoluzione della gerarchia}
\end{figure}

Vedremo comunque che nella traduzione dallo schema concettuale a quello logico,
sarà necessario aggiungere una tabella "Optional" per specificare quali optional
sono stati montati durante la realizzazione di un'arma.

### Scelta delle chiavi

#### Chiavi primarie
Tutte le entità presenti in *Figura \ref{ER}* sono già dotate di chiave primaria
(indicata dal pallino pieno). Inoltre, alle associazioni con cardinalità
**(n,m)**, che in seguito verranno tradotte in relazioni, utilizzeranno come
chiave primaria l'insieme degli identificatori delle entità alle quali sono
collegate.

#### Chiavi esterne
Le associazioni con cardinalità **(1,n)** verranno tradotte successivamente in
chiavi esterne, nell'entita di cardinalità **1**. Tutte le chiavi esterne
saranno denominate secondo la relazione alla quale fanno riferimento. Il valore
della chiave esterna corrisponde chiaramente a quello della chiave primaria
della relazione di riferimento.
Le chiavi primarie delle associazioni con cardinalità **(n,m)** sono allo stesso
tempo anche chiavi esterne delle rispettive relazioni omonime.

## Analisi delle ridondanze
Una delle operazioni principali dell'applicazione è quella di restituire i
maretiali necessari alla costruzione di un'arma data in input dall'utente.
Siccome questa procedura coinvolge *ARMI*, *COMPONENTI* e *MATERIALI*, sarebbe
utile avere un'associazione *(UTILIZZO)* ridondante che colega direttamente
un'arma ai suoi materiali *(Figura \ref{ERMCW_R})*.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.4]{source/screen/ERMCW_R.png}
    \caption{Introduzione di ridondanza}
    \label{ERMCW_R}
\end{figure}

L'unica operazione influenzata da questa ridondanza è quella descritta al
paragrafo 3.3.3 (**Visualizzazione dei materiali necessari alla costruzione di
un'arma**).

Sappiamo che in assenza di ridondanza quest'operazione ha un costo di 4100
operazioni al giorno. Vediamo ora quale sarebbe il suo costo se venisse
introdotta la ridondanza. Teniamo a mente che l'associazione *UTILIZZO* avrebbe
un volume pari a *35 (armi) x 16 (materiali per arma) = 560*

|    Concetto   | Costrutto | Accessi    | Tipo (Lettura/Scrittura) |
|:-------------:|:---------:|:----------:|:------------------------:|
|      Arma     |     E     |     1      |             L            |
|   Utilizzo    |     R     |    16      |             L            |
|   Materiale   |     E     |    16      |             L            |

: Numero di accessi in presenza di ridondanza

**Totale**: 33L

**Frequenza**: 100/gg

**Costo totale**: 33 * 100 = 3300/gg

Alla fine, nonostante il miglioramento in termini di efficenza, si è deciso di
**non** introdurre la ridondanza. Questa decisione deriva prevalentemente dal
sostanzioso volume di dati che verrebbe introdotto.


## Traduzione di entità e associazioni in relazioni

**Utente**(\underline{\textbf{username}}, password, e-mail, nome, cognome, via, numero, citta,
stato, cap)

**Inventario**(\underline{\textbf{proprietario}}, \underline{\textbf{materiale}}, quantita) \newline
FK: proprietario REFERENCE **Utente** \newline
FK: materiale REFERENCE **Materiale**

**Materiale**(\underline{\textbf{codM}}, nome, descrizione, prezzo, img)

**Ingredienti**(\underline{\textbf{materiale}}, \underline{\textbf{componente}}, quantita) \newline
FK: materiale REFERENCE **Materiale** \newline
FK: componente REFERENCE **Componente**

**Componente**(\underline{\textbf{codP}}, arma, nome, img, tipo) \newline
FK: arma REFERENCE **Arma** \newline
FK: tipo REFERENCE **Tipo**

**Tipo**(\underline{\textbf{codT}}, ruolo)

**Composizione**(\underline{\textbf{tipo}}, \underline{\textbf{categoria}})
\newline
FK: tipo REFERENCE **Tipo** \newline
FK: categoria REFERENCE **Categoria**

**Categoria**(\underline{\textbf{codC}}, nome)

**Arma**(\underline{\textbf{codF}}, nome, categoria, img) \newline
FK: categoria REFERENCE **Categoria**

**Optional**(\underline{\textbf{real}}, \underline{\textbf{tipo}}, codice)
\newline
FK: real REFERENCE **Realizzata** \newline
FK: codice REFERENCE **Componente**

**Realizzata**(\underline{\textbf{progressivo}}, utente, arma) \newline
FK: utente REFERENCE **Utente** \newline
FK: arma REFERENCE **Arma**

**Inventario**(\underline{\textbf{proprietario}}, \underline{\textbf{materiale}}, quantita) \newline
FK: proprietario REFERENCE **Utente** \newline
FK: materiale REFERENCE **Materiale**

**Competenza**(\underline{\textbf{abilita}}, \underline{\textbf{utente}}, livello) \newline
FK: abilita REFERENCE **Abilita** \newline
FK: utente REFERENCE **Utente**

**Abilita**(\underline{\textbf{codA}}, nome)

**RichiestaA**(\underline{\textbf{abilita}}, \underline{\textbf{arma}}, livello)
\newline
FK: abilita REFERENCE **Abilita** \newline
FK: arma REFERENCE **Arma**

**Associazione**(\underline{\textbf{arma}}, \underline{\textbf{stat}}, livello)
\newline
FK: arma REFERENCE **Arma** \newline
FK: stat REFERENCE **Statistica**

**Statistica**(\underline{\textbf{codS}}, nome)
\newpage

## Costruzione tabelle del DB in linguaggio SQL
### Creazione tabella **Utente**

```sql
CREATE TABLE User(username varchar(20) PRIMARY KEY,
password varchar(50), email varchar(255), name varchar(20),
surname varchar(20), street varchar(20), civic integer,
city varchar(20), country varchar(20), zip integer);
```

### Creazione tabella **Materiale**

```sql
CREATE TABLE Material(codM INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(20) NOT NULL, description VARCHAR(200),
price DOUBLE NOT NULL, image VARCHAR(255) NOT NULL);
```

### Creazione tabella **Inventario**

```sql
CREATE TABLE Inventory(owner VARCHAR(20), material INT,
quantity INT NOT NULL, PRIMARY KEY(owner, material),
FOREIGN KEY(owner) REFERENCES User(username),
FOREIGN KEY(material) REFERENCES Material(codM));
```

### Creazione tabella **Categoria**

```sql
CREATE TABLE Category(codC INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(20) NOT NULL);
```

\newpage
### Creazione tabella **Arma**

```sql
CREATE TABLE Weapon(codW INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(20) NOT NULL, category INT NOT NULL,
image VARCHAR(255) NOT NULL, FOREIGN KEY(category)
REFERENCES Category(codC));
```

### Creazione tabella **Tipo**

```sql
CREATE TABLE Type(codT INT AUTO_INCREMENT PRIMARY KEY,
role VARCHAR(20));
```

### Creazione tabella **Composizione**

```sql
CREATE TABLE Composition(type INT NOT NULL,
category INT NOT NULL, PRIMARY KEY(type, category),
FOREIGN KEY(type) REFERENCES Type(codT),
FOREIGN KEY(category) REFERENCES Category(codC));
```

### Creazione tabella **Statistica**

```sql
CREATE TABLE Statistic(codS INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(20));
```

### Creazione tabella **Associatione**

```sql
CREATE TABLE Association(weapon INT, statistic INT,
level INT NOT NULL, PRIMARY KEY(weapon, statistic),
FOREIGN KEY(weapon) REFERENCES Weapon(codW),
FOREIGN KEY(statistic) REFERENCES Statistic(codS));
```
### Creazione tabella **Componente**

```sql
CREATE TABLE Component(codComp INT AUTO_INCREMENT PRIMARY KEY,
weapon INT, name VARCHAR(20) NOT NULL, image VARCHAR(255),
type INT NOT NULL, FOREIGN KEY(weapon) REFERENCES Weapon(codW),
FOREIGN KEY(type) REFERENCES Type(codT));
```

### Creazione tabella **Ingredienti**

```sql
CREATE TABLE Ingredient(material INT NOT NULL, 
component INT NOT NULL, quantity INT NOT NULL,
PRIMARY KEY(material, component), FOREIGN KEY(material)
REFERENCES Material(codM), FOREIGN KEY(component)
REFERENCES Component(codComp));
```

### Creazione tabella **Realizzata**

```sql
CREATE TABLE Realization(codR INT AUTO_INCREMENT PRIMARY KEY,
user VARCHAR(20) NOT NULL, weapon INT NOT NULL,
FOREIGN KEY(user) REFERENCES User(username),
FOREIGN KEY(weapon) REFERENCES Weapon(codW));
```

### Creazione tabella **Optional**

```sql
CREATE TABLE Optional(realization INT NOT NULL, type INT NOT NULL,
component INT NOT NULL, PRIMARY KEY(realization, type),
FOREIGN KEY(realization) REFERENCES Realization(codR),
FOREIGN KEY(type) REFERENCES Type(codT), 
FOREIGN KEY(component) REFERENCES Component(codComp));
```

### Creazione tabella **Abilita**

```sql
CREATE TABLE Skill(codA INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(20) NOT NULL);
```

### Creazione tabella **Competenza**

```sql
CREATE TABLE Ability(skill INT NOT NULL, user VARCHAR(20) NOT NULL,
level INT NOT NULL, PRIMARY KEY(skill, user),
FOREIGN KEY(skill) REFERENCES Skill(codS),
FOREIGN KEY(user) REFERENCES User(username));
```

### Creazione tabella **RichiestaA**

```sql
CREATE TABLE RequirementW(skill INT NOT NULL, weapon INT NOT NULL,
level INT NOT NULL, PRIMARY KEY(skill, weapon),
FOREIGN KEY(skill) REFERENCES Skill(codS),
FOREIGN KEY(weapon) REFERENCES Weapon(codW));
```

### Creazione tabella **RichiestaC**

```sql
CREATE TABLE RequirementC(skill INT NOT NULL, component INT NOT NULL,
level INT NOT NULL, PRIMARY KEY(skill, component),
FOREIGN KEY(skill) REFERENCES Skill(codS),
FOREIGN KEY(component) REFERENCES Component(codComp));
```

## Schema relazionale finale

\begin{figure}[H]
  \begin{minipage}{0.48\textwidth}
    \centering
    \includegraphics[scale=0.65]{source/screen/Progetto-Logico2.png}
  \end{minipage}\hfill
  \caption{Schema logico finale}
\end{figure}
\newpage

## Traduzione delle operazioni in query SQL

### Lettura dell'inventario di un utente

```sql
select Material.name, Inventory.quantity
from Inventory
join Material on Material.codM = Inventory.material
where Inventory.owner = 'user'
```

### Acquisto di un materiale da parte di un utente

In caso il materiale non sia già presente nel suo inventario è da aggiungere:

```sql
insert into Inventory
(Inventory.owner, Inventory.material, Inventory.quantity)
values ('userName', 'material', quantity)
```

Se è già presente, si deve incrementare la quantità

```sql
update Inventory
set Inventory.quantity = Inventory.quantity + 1
where Inventory.material = select Material.codM
                            from Material
                            where Material.name = 'materialName'
and Inventory.owner = 'username'
```

### Visualizzazione di tutti i materiali

```sql
select Material.name, Material.price
from Material
```

### Autenticazione di un utente

```sql
select User.name
from User
where User.name = 'username' AND User.password = 'password'
```

### Registrazione di un nuovo utente nel DB

```sql
insert into user
values ('username', 'password', 'name',
'surname', 'email', 'street',
'civic', 'zip', 'city', 'country')
```

### Lettura di tutte le categorie

```sql
select Category.name
from Category
```

### Visualizzazione armi filtrate per categoria

```sql
select Weapon.name, Weapon.image
from Weapon
join Category on Weapon.category = Category.codC
where Category.name = 'category'
```

### Armi possedute da uno specifico utente

```sql
select Weapon.name
from Weapon
join Realization on Weapon.codW = Realization.weapon
where Realization.user = 'username'
```
### Visualizzazione dei materiali necessari alla costruzione di un'arma

```sql
select Material.name, sum(Ingredient.quantity), sum(Material.price)
from Material
join Ingredient on Material.codM = Ingredient.material
join Component on Ingredient.component = Component.codComp
join Weapon on Component.weapon = 'weapon'
group by Material.name
```

### Visualizzazione requisiti arma

```sql
select Skill.name Requirementw.level
from Weapon
join Association on Weapon.codW = Association.weapon
join Statistic on Association.statistic = Statistic.codS
where Weapon.name = 'weaponName'
```

### Visualizzazione statistiche arma

```sql
select Statistic.name, Association.level
from Weapon
join Association on Weapon.codW = Association.weapon
join Statistic on Association.statistic = Statistic.codS
where Weapon.name = 'weaponName'
```
\newpage

### Aggiornamento inventario a seguito di costruzione arma

```sql
update Inventory
join Material on Inventory.material = Material.codM
set Inventory.quantity, Inventory.quantity = qty
where Material.name = 'materialName'
and Inventory.owner = 'username'
```

### Inserimento nuova arma per utente

```sql
insert into Realization
values ('username',
select Weapon.codW
from Weapon
where Weapon.name = 'weaponName')
```

\newpage
