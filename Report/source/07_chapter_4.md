# Progettazione dell'applicazione

In questo capitolo viene descritto in dettaglio l'architettura dell'applicazione
che si interfaccia al Database. Si esaminerà l'interfaccia grafica descrivendone
tutte le funzionalità messe a disposizione all'utente; si esaminerà poi il
driver utilizzato per l'interfacciamento al DB.

## Descrizione architettura software

L'applicazione è stata realizzata in linguaggio Java per poter consentire
l'esecuzione su più piattaforme (diversi sistemi operativi).
Ci si è concentrati nello sviluppare una GUI
estremente semplice e immediata per l'utente, cercando di rendere di facile
utilizzo l'applicazione.

Per l'interfacciamento al DB si è utilizzato il framework **JOOQ** che
semplifica notevolmente tutta la gestione delle query riducendo al minimo le
possibilità che si verifichino conversioni di tipo (da SQL a Java e viceversa)
errate. Inoltre **JOOQ** fornisce i risultati sotto forma di stream Java
rendendo estrememente agevole e agile la manipolazione e l'utilizzo di records
in output alla query.

## Vantaggi nell'utilizzo di JOOQ

Durante la fase di analisi degli strumenti da utilizzare nell'applicazione si è
appurato che il driver JDBC offre uno scarno supporto al programmatore
intenzionato ad operare sul DB: i record ritornati molto spesso non hanno una
corrispondenza univolca con i dati standard in Java, quindi talvolta è
necessario dover optare per conversioni pericolose. D'altro canto JOOQ risolve
completamente tutte le problematiche osservate in JDBC: tutti i record che le
query ritornano sono *type safe*: ciò significa che si opera solamente con i
tipi di dato classici di Java, in quanto tutte le conversioni del caso vengono
effettuate direttamente da JOOQ. Inoltre un grande vantaggio portato da JOOQ è
la possibilità di utilizzare le API fluenti tipiche di Java 8 per effettuare le
query e non doverle scrivere sotto forma di stringa (modalità che ha
intrinsecamente una propensione ad errori sintattici e non solo). JOOQ ha di
default la predisposizione all'utilizzo degli stream sui risultati prodotti 
alle query, semplificando enormemente la manipolazione e la gestione dei record.

Un altro vantaggio caratterizzante di JOOQ è la possibilità di effettuare un
_dump_ del DB e generare classi Java che rappresentino le tabelle del DB,
includendo tutte le porprietà necessarie e metodi di utilità. Questa
funzionalità consente di ridurre i tempi di sviluppo rendendo meno predisposto
ad errori il software.

Il driver risulta essere molto completo sia in termini di statement SQL che di
funzionalità messe a disposizione al programmatore. Nel contesto applicativo in
cui è stato utilizzato è risultato molto comodo e veloce, coprendo tutte le
esigenze che il nostro dominio richiedeva.

\newpage
## Descrizione funzionamento applicazione

L'applicativo all'avvio presenta una schermata di **login** con cui gli utenti
possono accedere al gestionale: l'username identifica univocamente l'utente
presente nel database e la password viene richiesta per ragioni di sicurezza.
Qualora l'utente non fosse presente, un pop-up avverte l'utente che username e
password sono errati. Sempre in qusta schermata è presente un pulsante per la
registrazione di un nuovo utente; cliccando sul pulsante l'applicativo propone
all'utente una form da compilare per la registrazione del nuovo utente. A
seguito della registrazione verrà memorizzato nel DB il nuovo utente e
conseguentemente potrà effettuare il login.

\begin{figure}[h]
  \begin{minipage}{0.48\textwidth}
    \centering
    \includegraphics[scale=0.5]{source/screen/loginview.png}
    \caption{Login view}
  \end{minipage}\hfill
  \begin{minipage}{0.48\textwidth}
    \centering
    \includegraphics[scale=0.5]{source/screen/registerview.png}
    \caption{Register view}
  \end{minipage}
\end{figure}

Una volta effettuato il login l'utente ha a disposizione diverse sezioni
interagibili: sulla parte alta a sinistra trova tutte le categorie di armi e,
per ogni categoria, sono indicate tutte le relative armi che è possibile
costruire; sulla parte bassa a sinistra trova invece l'inventario dei
materiali di cui dispone, con i quali possono essere costruite le armi. Nella
parte centrale sono indicati tutti i materiali presenti nel DB che servono alla
costruzione delle armi. Infine sulla parte destra è presente un carrello che
visualizza i materiali attualmente presenti nel carrello e che possono essere
acquistati. L'acquisto dei materiali comporta l'aggiornamento delle relative
tabelle e conseguentemente l'aggiornamento dell'inventario. L'acquisto dei
materiali viene effettuato mediante il pulsante **Buy**, il quale esegue la
query per aggiungere all'inventario i materiali appena acquistati.

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.6]{source/screen/mainview.png}
  \caption{Main view application}
\end{figure}

L'utente che desidera costruire un'arma, seleziona la categoria di appartenenza:
a questo punto l'applicazione mostra tutte le armi presenti nel database
appartenenti a quella categoria. Cliccando sull'arma desiderata appare
un pop-up che mostra tutti i materiali necessari alla costruzione oltre alle
statistiche dell'arma e i requisiti per l'arma. Due pulsanti consentono
l'interazione con il pop-up: **Add material** e **craft**.
Il primo consente di aggiungere all'inventario i materiali richiesti per l'arma
senza dover selezionare manualmente ogni singolo materiale dalla schermata
principale; mentre **craft** consente di acquistare direttamente l'arma senza
acqistare i materiali. La realizzazione dell'arma può avvenire solamente se
l'utente dispone di sufficienti materiali per la costruzione, in caso contrario
il pulsante è disabilitato e non è possibile costruire l'arma.

\begin{figure}[H]
  \centering
  \includegraphics[scale=1]{source/screen/weapon-details.png}
  \caption{Pop-up acquisto materiali per costruzione arma}
\end{figure}

Per acquistare i singoli materiali l'utente ha a disposizione la sezione
*Material* in cui sono elencati tutti i materiali disponibili. Cliccando su
un materiale si apre un pop-up che chiede la quantità che si desidera di questo
materiale, confermando con **OK** quest'ultimo viene aggiunto al carrello;
qualora il materiale fosse già presente nel carrello, verrà aggiornata
solamente la quantità corrispondente.

\begin{figure}[H]
  \centering
  \includegraphics[scale=1.5]{source/screen/material-add.png}
  \caption{Pop-up acquisto materiali}
\end{figure}

Nella parte inferiore dell'applicazione oltre all'inventario dei materiali già
descritto in precedenza troviamo l'elenco di tutte le armi possedute
dall'utente, questo elenco non presenta alcuna interazione con l'utente ma ha
solo la funzione di mostrare tutte le armi possedute.

## Implementazioni future

In una futura estensione dell'applicativo sono previste l'introduzione di altre
funzionalita:

- Possibilità di aggiungere optional all'arma che si vuole costruire
- Rimozione di un singolo elemento dal carrello
- Sezione dedicata all'utente per la gestione profilo
- Utilizzo di immagini per una grafica più accattivante.

Tutte le implementazioni ancora non realizzate, non sono state realizzate per
motivi di tempo, la loro implementazione non richiede alcuna modifica al DB in
quanto è stato progettato per supportare tutte le funzionalità appena citate.
