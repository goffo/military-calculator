/*
 * query.sql
 * Copyright (C) 2018 nicolasfara <nicolas.farabegoli@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

select Material.name, Inventory.quantity
from Inventory
join Material on Material.codM = Inventory.material
where Inventory.owner = 'user'

update Inventory
set Inventory.quantity = Inventory.quantity + 1
where Inventory.material = select Material.codM from Material where Material.name = 'materialName'

insert into Inventory (Inventory.owner, Inventory.material, Inventory.quantity)
values ('userName', 'material', quantity)

select Material.name, Material.price
from Material

select User.name 
from User
where User.name = 'username' AND User.password = 'password'

insert into user
values ('username', 'password', 'name', 'suername', 'email', 'street', 'civic', 'zip', 'city', 'country')

select Category.name
from Category

select Weapon.name, Weapon.image
from Weapon
join Category on Weapon.category = Category.codC
where Category.name = 'category'

select Weapon.name
from Weapon
join Realization on Weapon.codW = Realization.weapon
where Realization.user = 'username'

select Material.name, sum(Ingredient.quantity), sum(Material.price)
from Material
join Ingredient on Material.codM = Ingredient.material
join Component on Ingredient.component = Component.codComp
join Weapon on Component.weapon = 'weapon'
group by Material.name

select Skill.name Requirementw.level
from Weapon
join Association on Weapon.codW = Association.weapon
join Statistic on Association.statistic = Statistic.codS
where Weapon.name = 'weaponName'

select Statistic.name, Association.level
from Weapon
join Association on Weapon.codW = Association.weapon
join Statistic on Association.statistic = Statistic.codS
where Weapon.name = 'weaponName'

-- vim:et
