#! /bin/sh
#
# build.sh
# Copyright (C) 2018 nicolasfara <nicolas.farabegoli@gmail.com>
#
# Distributed under terms of the MIT license.
#

TERM=xterm-color ./gradlew clean customFatJar
