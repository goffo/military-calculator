package it.unibo.db;

import it.unibo.db.query.InventoryQuery;
import it.unibo.db.user.User;
import it.unibo.db.weapon.Material;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Inventory {

    private ObservableList<Material> inventoryList = FXCollections.observableArrayList(new ArrayList<>());
    private User user = User.getUser();
    private InventoryQuery inventoryQuery = new InventoryQuery();

    public Inventory() {
        update();
    }

    public void update() {
        inventoryList.clear();
        inventoryList.addAll(inventoryQuery.getInventoryByUser(user.getName()));
    }

    public ObservableList<Material> getInventoryList() {
        return inventoryList;
    }

    public void addToInventory(final Material material) {
        inventoryList.add(material);
    }

    public void addToInventory(final List<Material> listMaterial) {
        inventoryList.addAll(listMaterial);
    }

    public List<String> getAllMaterial() {
        return inventoryList.stream()
                .map(Material::getMaterialName)
                .collect(Collectors.toList());
    }

    public Optional<Integer> getQuantityByMaterial(final String materialName) {
        return inventoryList.stream()
                .filter(e -> e.getMaterialName().equals(materialName))
                .map(Material::getQuantity)
                .findFirst();
    }
}
