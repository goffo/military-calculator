package it.unibo.db;

import it.unibo.db.connector.ConnectionSingleton;
import it.unibo.db.connector.ConnectorDB;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.Connection;

/**
 * The main class where everything start.
 */
public final class MainViewApplication extends Application {

    private static final double MIN_HEIGHT = 720;
    private static final double MIN_WIDTH = 640;
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String UTC = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private Connection connection;

    /**
     * The main method, launch the application.
     * @param args command line args.
     */
    public static void main(final String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        primaryStage.setTitle("Military calculator");
        primaryStage.setMinHeight(MIN_HEIGHT);
        primaryStage.setMinWidth(MIN_WIDTH);

        final ConnectorDB connectorDB = ConnectionSingleton.getConnector();
        connectorDB.loadDriver(JDBC_DRIVER);
        connection = connectorDB.getConnection();

        /*try {
            DriverManager.setLoginTimeout(4);
            //connection
        } catch (SQLException comEx) {
            Alert alert = new AlertManager.AlertBuilder().setAlertType(Alert.AlertType.ERROR).addButtonType(ButtonType.CLOSE).setMessage("Server not found").build();
            alert.showAndWait();
        }*/


        //DSLContext create = DSL.using(connection, SQLDialect.MYSQL);
        //Result<Record> result = create.select()
        //                                .from(PERSON)
        //                                .where(PERSON.ID.eq(788928))
        //                                .fetch();
        //if (result != null) {
        //    result.forEach(e -> {
        //        System.out.println("ID:" + e.getValue(PERSON.ID) + " Name: " + e.getValue(PERSON.NAME) + " Surname: " + e.getValue(PERSON.SURNAME));
        //    });
        //}

        Parent loginNode = FXMLLoader.load(getClass().getResource("/fxml/connection/LoginView.fxml"));
        Scene loginScene = new Scene(loginNode, MIN_WIDTH, MIN_HEIGHT);
        primaryStage.setScene(loginScene);
        primaryStage.show();
    }
}
