package it.unibo.db.alert;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class AlertManager extends Alert {

    private List<ButtonType> buttonTypes = new ArrayList<>();
    private AlertType alertType;
    private String message;

    public AlertManager(final AlertType alertType) {
        super(alertType);
    }

    public AlertManager(final AlertType alertType, final String contentText, final ButtonType... buttons) {
        super(alertType, contentText, buttons);
    }

    /**
     *
     */
    public static class AlertBuilder {
        private String message;
        private List<ButtonType> buttonTypeList = new ArrayList<>();
        private AlertType alertType;
        private boolean built= false;
        /**
         *
         * @param buttonType
         * @return
         */
        public AlertBuilder addButtonType(final ButtonType buttonType) {
            buttonTypeList.add(buttonType);
            return this;
        }

        /**
         *
         * @param alertType
         * @return
         */
        public AlertBuilder setAlertType(final AlertType alertType) {
            this.alertType = alertType;
            return this;
        }

        /**
         *
         * @param message
         * @return
         */
        public AlertBuilder setMessage(final String message) {
            this.message = message;
            return this;
        }

        /**
         *
         * @return
         */
        public AlertManager build() {
            if (!built) {
                built = true;
                return new AlertManager(this.alertType, this.message, this.buttonTypeList.toArray(new ButtonType[buttonTypeList.size()]));
            }
            throw new IllegalStateException("Unable to call build() twice");
        }
    }
}
