package it.unibo.db.connector;

/**
 *
 */
public class ConnectionSingleton {
    private static ConnectorDB connection;
    private static final String USERNAME = "user";
    private static final String PASSWORD = "pswd";

    private ConnectionSingleton() { }

    /**
     * .
     * @return .
     */
    public static ConnectorDB getConnector() {
        if (connection == null) {
            connection = new ConnectorDBImpl(USERNAME, PASSWORD);
            return connection;
        }
        return connection;
    }
}
