package it.unibo.db.connector;

import java.sql.Connection;

/**
 *
 */
public interface ConnectorDB {
    /**
     * This methods try to load the driver.
     * @param driver driver name following the name convention for JDBC.
     */
    void loadDriver(String driver);

    /**
     * This method get a connection to the DB.
     * @return the connection at DB instance.
     */
    Connection getConnection();
}
