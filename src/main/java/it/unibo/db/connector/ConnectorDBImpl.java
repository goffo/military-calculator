package it.unibo.db.connector;

import it.unibo.db.alert.AlertManager;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * This class is used for testing a DB connection and return a connection instance.
 */
public class ConnectorDBImpl implements ConnectorDB {

    private static final String UTC = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String URL = "jdbc:mysql://88.147.17.142:3306/";
    private static final String DB_NAME = "military";
    private final String userName;
    private final String password;

    ConnectorDBImpl(final String userName, final String password) {
        this.userName = userName;
        this.password = password;
    }

    @Override
    public void loadDriver(final String driver) {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException classException) {
            System.err.println("The driver: " + driver + " is not found");
            classException.printStackTrace();

            AlertManager alert = new AlertManager.AlertBuilder()
                    .setAlertType(Alert.AlertType.ERROR)
                    .addButtonType(ButtonType.OK)
                    .setMessage(classException.toString())
                    .build();
            alert.showAndWait();
            throw new IllegalStateException();
        }
    }

    @Override
    public Connection getConnection() {
        Connection connection;
        try {
            connection = DriverManager.getConnection(URL + DB_NAME + UTC, userName, password);
        } catch (SQLException sqlException) {
            AlertManager alert = new AlertManager.AlertBuilder()
                    .setMessage(sqlException.toString())
                    .addButtonType(ButtonType.OK)
                    .setAlertType(Alert.AlertType.ERROR)
                    .build();
            alert.showAndWait();
            throw new IllegalStateException();
        }
        return connection;
    }
}
