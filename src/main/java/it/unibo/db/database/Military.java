/*
 * This file is generated by jOOQ.
*/
package it.unibo.db.database;


import it.unibo.db.database.tables.Ability;
import it.unibo.db.database.tables.Association;
import it.unibo.db.database.tables.Category;
import it.unibo.db.database.tables.Component;
import it.unibo.db.database.tables.Composition;
import it.unibo.db.database.tables.Ingredient;
import it.unibo.db.database.tables.Inventory;
import it.unibo.db.database.tables.Material;
import it.unibo.db.database.tables.Optional;
import it.unibo.db.database.tables.Realization;
import it.unibo.db.database.tables.Requirementc;
import it.unibo.db.database.tables.Requirementw;
import it.unibo.db.database.tables.Skill;
import it.unibo.db.database.tables.Statistic;
import it.unibo.db.database.tables.Type;
import it.unibo.db.database.tables.User;
import it.unibo.db.database.tables.Weapon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Military extends SchemaImpl {

    private static final long serialVersionUID = 2145452576;

    /**
     * The reference instance of <code>military</code>
     */
    public static final Military MILITARY = new Military();

    /**
     * The table <code>military.Ability</code>.
     */
    public final Ability ABILITY = it.unibo.db.database.tables.Ability.ABILITY;

    /**
     * The table <code>military.Association</code>.
     */
    public final Association ASSOCIATION = it.unibo.db.database.tables.Association.ASSOCIATION;

    /**
     * The table <code>military.Category</code>.
     */
    public final Category CATEGORY = it.unibo.db.database.tables.Category.CATEGORY;

    /**
     * The table <code>military.Component</code>.
     */
    public final Component COMPONENT = it.unibo.db.database.tables.Component.COMPONENT;

    /**
     * The table <code>military.Composition</code>.
     */
    public final Composition COMPOSITION = it.unibo.db.database.tables.Composition.COMPOSITION;

    /**
     * The table <code>military.Ingredient</code>.
     */
    public final Ingredient INGREDIENT = it.unibo.db.database.tables.Ingredient.INGREDIENT;

    /**
     * The table <code>military.Inventory</code>.
     */
    public final Inventory INVENTORY = it.unibo.db.database.tables.Inventory.INVENTORY;

    /**
     * The table <code>military.Material</code>.
     */
    public final Material MATERIAL = it.unibo.db.database.tables.Material.MATERIAL;

    /**
     * The table <code>military.Optional</code>.
     */
    public final Optional OPTIONAL = it.unibo.db.database.tables.Optional.OPTIONAL;

    /**
     * The table <code>military.Realization</code>.
     */
    public final Realization REALIZATION = it.unibo.db.database.tables.Realization.REALIZATION;

    /**
     * The table <code>military.RequirementC</code>.
     */
    public final Requirementc REQUIREMENTC = it.unibo.db.database.tables.Requirementc.REQUIREMENTC;

    /**
     * The table <code>military.RequirementW</code>.
     */
    public final Requirementw REQUIREMENTW = it.unibo.db.database.tables.Requirementw.REQUIREMENTW;

    /**
     * The table <code>military.Skill</code>.
     */
    public final Skill SKILL = it.unibo.db.database.tables.Skill.SKILL;

    /**
     * The table <code>military.Statistic</code>.
     */
    public final Statistic STATISTIC = it.unibo.db.database.tables.Statistic.STATISTIC;

    /**
     * The table <code>military.Type</code>.
     */
    public final Type TYPE = it.unibo.db.database.tables.Type.TYPE;

    /**
     * The table <code>military.User</code>.
     */
    public final User USER = it.unibo.db.database.tables.User.USER;

    /**
     * The table <code>military.Weapon</code>.
     */
    public final Weapon WEAPON = it.unibo.db.database.tables.Weapon.WEAPON;

    /**
     * No further instances allowed
     */
    private Military() {
        super("military", null);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Table<?>> getTables() {
        List result = new ArrayList();
        result.addAll(getTables0());
        return result;
    }

    private final List<Table<?>> getTables0() {
        return Arrays.<Table<?>>asList(
            Ability.ABILITY,
            Association.ASSOCIATION,
            Category.CATEGORY,
            Component.COMPONENT,
            Composition.COMPOSITION,
            Ingredient.INGREDIENT,
            Inventory.INVENTORY,
            Material.MATERIAL,
            Optional.OPTIONAL,
            Realization.REALIZATION,
            Requirementc.REQUIREMENTC,
            Requirementw.REQUIREMENTW,
            Skill.SKILL,
            Statistic.STATISTIC,
            Type.TYPE,
            User.USER,
            Weapon.WEAPON);
    }
}
