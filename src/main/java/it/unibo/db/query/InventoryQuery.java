package it.unibo.db.query;

import it.unibo.db.weapon.Material;
import it.unibo.db.weapon.MaterialImpl;
import org.jooq.Record2;
import org.jooq.Result;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static it.unibo.db.database.Tables.MATERIAL;
import static it.unibo.db.database.tables.Inventory.INVENTORY;

public class InventoryQuery extends QueryManager {

    public InventoryQuery() {
        super();
    }

    public List<Material> getInventoryByUser(final String user) {
        Result<Record2<String, Integer>> result = super.query.select(MATERIAL.NAME, INVENTORY.QUANTITY)
                .from(INVENTORY)
                .join(MATERIAL)
                .on(MATERIAL.CODM.eq(INVENTORY.MATERIAL))
                .where(INVENTORY.OWNER.eq(user))
                .fetch();

        return result.stream()
                .map(e -> new MaterialImpl(e.component1(), BigDecimal.valueOf(0), BigDecimal.valueOf(e.component2())))
                .collect(Collectors.toList());

    }
}
