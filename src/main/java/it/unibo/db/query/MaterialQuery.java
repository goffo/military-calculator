package it.unibo.db.query;

import it.unibo.db.user.User;
import it.unibo.db.utility.Utility;
import it.unibo.db.weapon.Material;
import it.unibo.db.weapon.MaterialImpl;
import org.jooq.Record2;
import org.jooq.Result;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static it.unibo.db.database.tables.Inventory.INVENTORY;
import static it.unibo.db.database.tables.Material.MATERIAL;

public class MaterialQuery extends QueryManager {
    private User user = User.getUser();

    public MaterialQuery() {
        super();
    }

    public int buyMaterial(final String materialName, final Integer quantity) {
        return super.query.update(INVENTORY)
                    .set(INVENTORY.QUANTITY, INVENTORY.QUANTITY.add(quantity))
                    .where(INVENTORY.MATERIAL.eq(
                            query.select(MATERIAL.CODM)
                            .from(MATERIAL)
                            .where(MATERIAL.NAME.eq(materialName))))
                    .and(INVENTORY.OWNER.eq(user.getName()))
                    .execute();
    }

    public int addNewMaterialUser(final String userName, final Material material) {
        return super.query.insertInto(INVENTORY, INVENTORY.OWNER, INVENTORY.MATERIAL, INVENTORY.QUANTITY)
                        .values(userName, Utility.materialNameToCode(material), material.getQuantity())
                        .execute();
    }

    public List<Material> getAllMaterial() {
        Result<Record2<String, Double>> result = super.query.select(MATERIAL.NAME, MATERIAL.PRICE)
                .from(MATERIAL)
                .fetch();

        return result.stream()
                .map(record -> new MaterialImpl(record.value1(), BigDecimal.valueOf(record.value2()), BigDecimal.valueOf(0)))
                .collect(Collectors.toList());
    }
}
