package it.unibo.db.query;

import it.unibo.db.connector.ConnectionSingleton;
import it.unibo.db.connector.ConnectorDB;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;

abstract class QueryManager {

    final DSLContext query;

    QueryManager() {
        ConnectorDB connectorDB = ConnectionSingleton.getConnector();
        Connection connection = connectorDB.getConnection();
        this.query = DSL.using(connection, SQLDialect.MYSQL);
    }
}
