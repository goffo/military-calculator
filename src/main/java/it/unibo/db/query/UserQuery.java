package it.unibo.db.query;

import org.jooq.Record1;
import org.jooq.Result;

import java.util.Optional;

import static it.unibo.db.database.tables.User.USER;

public class UserQuery extends QueryManager {

    public UserQuery() {
        super();
    }

    public Optional<String> getUserByLogin(final String userName, final String password) {
        Result<Record1<String>> user = super.query
                .select(USER.NAME)
                .from(USER)
                .where(USER.USERNAME.eq(userName))
                .and(USER.PASSWORD.eq(password))
                .fetch();

        if (user.isNotEmpty()) {
            return Optional.of(user.get(0).value1());
        } else {
            return Optional.empty();
        }
    }

    public int insertNewUser(String username, String password, String name, String surname, String email, String street, Integer number, Integer cap, String city, String country) {
        return super.query.insertInto(USER, USER.USERNAME, USER.PASSWORD, USER.NAME, USER.SURNAME, USER.EMAIL, USER.STREET, USER.CIVIC, USER.ZIP, USER.CITY, USER.COUNTRY)
                .values(username, password, name, surname, email, street, number, cap, city, country)
                .execute();
    }
}
