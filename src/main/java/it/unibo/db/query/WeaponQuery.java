package it.unibo.db.query;

import it.unibo.db.weapon.Material;
import it.unibo.db.weapon.MaterialImpl;
import it.unibo.db.weapon.Weapon;
import it.unibo.db.weapon.WeaponImpl;
import javafx.collections.ObservableList;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Result;
import org.jooq.impl.DSL;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static it.unibo.db.database.Tables.INVENTORY;
import static it.unibo.db.database.Tables.MATERIAL;
import static it.unibo.db.database.Tables.USER;
import static it.unibo.db.database.tables.Category.CATEGORY;
import static it.unibo.db.database.tables.Ingredient.INGREDIENT;
import static it.unibo.db.database.tables.Requirementw.REQUIREMENTW;
import static it.unibo.db.database.tables.Skill.SKILL;
import static it.unibo.db.database.tables.Weapon.WEAPON;
import static it.unibo.db.database.tables.Realization.REALIZATION;
import static it.unibo.db.database.tables.Ability.ABILITY;
import static it.unibo.db.database.tables.Component.COMPONENT;
import static it.unibo.db.database.tables.Realization.REALIZATION;
import static org.jooq.impl.DSL.sum;

public class WeaponQuery extends QueryManager {

    private InventoryQuery inventoryQuery = new InventoryQuery();

    public WeaponQuery() {
        super();
    }

    public List<String> getWeaponTabName() {
        Result<Record1<String>> tabName = super.query
                .select(CATEGORY.NAME)
                .from(CATEGORY)
                .fetch();

        return tabName.stream()
                .map(Record1::value1)
                .collect(Collectors.toList());
    }

    public List<Weapon> getWeaponFromCategory(final String category) {
        Result<Record2<String, String>> weapon = super.query.select(WEAPON.NAME, WEAPON.IMAGE)
              .from(WEAPON)
              .join(CATEGORY)
              .on(WEAPON.CATEGORY.eq(CATEGORY.CODC))
              .where(CATEGORY.NAME.eq(category))
              .fetch();

        return weapon.stream()
                .map(e -> {
                    Weapon weaponTmp = new WeaponImpl(e.component1());
                    weaponTmp.setWeaponImage(e.component2());
                    return weaponTmp;
                })
                .collect(Collectors.toList());
    }

    public List<String> getWeaponByUser(final String user) {
        Result<Record1<String>> weapon = super.query.select(WEAPON.NAME)
                .from(WEAPON)
                .join(REALIZATION)
                .on(WEAPON.CODW.eq(REALIZATION.WEAPON))
                .where(REALIZATION.USER.eq(user))
                .fetch();
        return weapon.stream()
                .map(Record1::value1)
                .collect(Collectors.toList());
    }

    public boolean checkAbilities(final String username, final String weapon) {
        Result<Record2<String, Integer>> weaponAbilities = query.select(SKILL.NAME, REQUIREMENTW.LEVEL)
                .from(SKILL)
                .join(REQUIREMENTW)
                .on(SKILL.CODS.eq(REQUIREMENTW.SKILL))
                .join(WEAPON)
                .on(REQUIREMENTW.WEAPON.eq(WEAPON.CODW))
                .where(WEAPON.NAME.eq(weapon))
                .fetch();

        Result<Record2<String, Integer>> userAbilities = super.query
                .select(SKILL.NAME, ABILITY.LEVEL)
                .from(SKILL)
                .join(ABILITY)
                .on(SKILL.CODS.eq(ABILITY.SKILL))
                .where(ABILITY.USER.eq(username))
                .fetch();

        for (Record2<String, Integer> res : weaponAbilities) {
            boolean out = userAbilities.stream()
                    .anyMatch(el -> el.component2() >= res.component2() && el.component1().equals(res.component1()));
            System.out.println(out);
            if (!out) {
                return false;
            }
        }

        return true;
    }

    public boolean checkMaterial(final String username, final String weapon) {
        Result<Record2<String, BigDecimal>> material = super.query.select(MATERIAL.NAME, sum(INGREDIENT.QUANTITY))
                .from(MATERIAL)
                .join(INGREDIENT).on(MATERIAL.CODM.eq(INGREDIENT.MATERIAL))
                .join(COMPONENT).on(INGREDIENT.COMPONENT.eq(COMPONENT.CODCOMP))
                .join(WEAPON).on(COMPONENT.WEAPON.eq(WEAPON.CODW))
                .where(WEAPON.NAME.eq(weapon))
                .groupBy(MATERIAL.NAME)
                .fetch();

        List<Material> materials = inventoryQuery.getInventoryByUser(username);

        for (Record2<String, BigDecimal> res : material) {
            boolean out = materials.stream()
                    .anyMatch(e -> e.getMaterialName().equals(res.component1()) && e.getQuantity() >= res.component2().intValue());
            if (!out) {
                return false;
            }
        }
        return true;
    }

    public void craftWeapon(final String username, final ObservableList<Material> materialList) {
        materialList.forEach(e -> {
            super.query
                    .update(INVENTORY.join(MATERIAL).on(INVENTORY.MATERIAL.eq(MATERIAL.CODM)))
                    .set(INVENTORY.QUANTITY, INVENTORY.QUANTITY.add(-e.getQuantity()))
                    .where(MATERIAL.NAME.eq(e.getMaterialName()))
                    .and(INVENTORY.OWNER.eq(username))
                    .execute();
        });
    }

    public void insertWeapon(final String username, final String weaponName) {
        query.insertInto(REALIZATION)
                .values(null, username, DSL.select(WEAPON.CODW).from(WEAPON).where(WEAPON.NAME.eq(weaponName)))
                .execute();
    }
}
