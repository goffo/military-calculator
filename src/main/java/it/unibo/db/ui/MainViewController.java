package it.unibo.db.ui;

import it.unibo.db.Inventory;
import it.unibo.db.query.InventoryQuery;
import it.unibo.db.query.MaterialQuery;
import it.unibo.db.query.WeaponQuery;
import it.unibo.db.user.User;
import it.unibo.db.weapon.Material;
import it.unibo.db.weapon.MaterialImpl;
import it.unibo.db.weapon.WeaponManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Controller for MainView.
 */
public final class MainViewController {

    @FXML private TabPane categoryTab;
    @FXML private TableView<Material> checkoutTable;
    @FXML private Label totalLabel;
    @FXML private TableColumn<Material, String> columnNameMaterial;
    @FXML private TableColumn<Material, Integer> columnQuantity;
    @FXML private TableColumn<Material, Double> columnPrice;
    @FXML private TableView<Material> inventoryTableView;
    @FXML private TableColumn<Material, String> inventoryMaterialName;
    @FXML private TableColumn<Material, Integer> inventoryQuantity;
    @FXML private FlowPane materialGrid;
    @FXML private ListView<String> weaponListView;
    @FXML private Label userLabel;
    private ObservableList<String> weaponList;
    private ObservableList<Material> materialList;
    private User user = User.getUser();
    private Inventory inventory = new Inventory();
    private final WeaponQuery weaponQuery = new WeaponQuery();
    private final MaterialQuery materialQuery = new MaterialQuery();

    /**
     * Default constructor for JavaFX.
     */
    @FXML public void initialize() {
        weaponQuery.getWeaponTabName().forEach(tabName -> {
            final Tab newTab = new Tab();
            newTab.setText(tabName);
            categoryTab.getTabs().add(newTab);
        });

        userLabel.setText(userLabel.getText() + user.getName());

        weaponList = FXCollections.observableArrayList(weaponQuery.getWeaponByUser(user.getName()));
        weaponListView.setItems(weaponList);


        //Initialize column TableView Cart
        columnNameMaterial.setCellValueFactory(new PropertyValueFactory<>("materialName"));
        columnPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        columnQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));

        //Initialize columns TableView Inventory
        inventoryMaterialName.setCellValueFactory(new PropertyValueFactory<>("materialName"));
        inventoryQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));

        inventoryTableView.setItems(inventory.getInventoryList());

        categoryTab.getSelectionModel().selectedItemProperty().addListener((observable, oldTab, newTab) -> {
            VBox vbox = new VBox();
            Label title = new Label("Weapons list");
            title.setFont(new Font(18));
            vbox.getChildren().add(title);
            vbox.setSpacing(25);
            vbox.setPadding(new Insets(25, 25, 25, 25));
            vbox.setAlignment(Pos.CENTER);
            FlowPane flowPane = new FlowPane();
            flowPane.setAlignment(Pos.CENTER);
            flowPane.setHgap(12);
            vbox.getChildren().add(flowPane);
            // Create a button for each result in the Query
            weaponQuery.getWeaponFromCategory(newTab.getText()).forEach(e -> {
                Button btn = new Button(e.getWeaponName());
                btn.setOnAction(event -> {
                    WeaponManager weaponManager = new WeaponManager(btn.getText());
                    List<Material> materialList = weaponManager.showAndConfirm();
                    if (materialList == null) {
                        materialList = Collections.emptyList();
                    }
                    this.materialList = FXCollections.observableArrayList(materialList);
                    //Add the list to TableView
                    this.materialList.addAll(checkoutTable.getItems());
                    checkoutTable.setItems(this.materialList);
                    calcTot();
                    inventory.update();
                    weaponList = FXCollections.observableArrayList(weaponQuery.getWeaponByUser(user.getName()));
                    weaponListView.setItems(weaponList);
                });
                flowPane.getChildren().add(btn);
            });
            // Add the FlowPane to the Tab's content
            newTab.setContent(vbox);
        });
        setupMaterialGrid();
    }

    @FXML void buyQuery() {
        for (Material mat : checkoutTable.getItems()) {
            int ret = materialQuery.buyMaterial(mat.getMaterialName(), mat.getQuantity());
            //if ret = 0 the material is not in the DB for this user
            if (ret == 0) {
                materialQuery.addNewMaterialUser(user.getName(), mat);
            }
        }
        //Update the table view
        inventory.update();
        clearCart();
    }

    @FXML void clearCart() {
        materialList.clear();
    }

    private void setupMaterialGrid() {
        materialQuery.getAllMaterial().forEach(material -> {
            Button btn = new Button(material.getMaterialName());
            btn.setOnAction(value -> {
                TextInputDialog textInputDialog = new TextInputDialog("1");
                textInputDialog.setTitle("Add Material");
                textInputDialog.setHeaderText("Add to cart your  material");
                textInputDialog.setContentText("Select quantity");
                textInputDialog.setResizable(true);
                Optional<String> result = textInputDialog.showAndWait();
                if (result.isPresent()) {
                    if (checkoutTable.getItems().stream()
                            .anyMatch(e -> e.getMaterialName().equals(material.getMaterialName()))) {
                        System.out.println("Founded");
                        Optional<Material> optionalMaterial = checkoutTable.getItems().stream()
                                .filter(e -> e.getMaterialName().equals(material.getMaterialName()))
                                .findFirst();
                        optionalMaterial.ifPresent(optMat -> optMat.addQuantity(Integer.parseInt(result.get())));
                        checkoutTable.refresh();
                    } else {
                        checkoutTable.getItems().add(new MaterialImpl(material.getMaterialName(),
                                BigDecimal.valueOf(material.getPrice()),
                                BigDecimal.valueOf(Integer.parseInt(result.get()))));
                    }
                }
            });
            materialGrid.getChildren().add(btn);
        });
    }

    private void calcTot() {
        if(checkoutTable.getItems().isEmpty()) {
            totalLabel.setText("0.00");
        }
        double total = checkoutTable.getItems().stream()
                .mapToDouble(Material::getPrice)
                .sum();
        totalLabel.setText(Double.toString(total));
    }
}
