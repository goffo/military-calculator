package it.unibo.db.ui.connection;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.GridPane;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionViewController {

    @FXML private GridPane leftGrid;
    @FXML private GridPane rightGrid;

    @FXML void initialize() throws  Exception {
        FXMLLoader leftLoader = new FXMLLoader(getClass().getResource("/fxml/connection/LoginView.fxml"));
        leftGrid.add(leftLoader.load(), 0, 0);
        FXMLLoader rightLoader = new FXMLLoader(getClass().getResource("/fxml/connection/RegisterView.fxml"));
        rightGrid.add(rightLoader.load(), 0, 0);
        Logger.getAnonymousLogger().log(Level.INFO, "Connection view loaded");
    }
}
