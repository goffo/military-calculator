package it.unibo.db.ui.connection;

import it.unibo.db.query.UserQuery;
import it.unibo.db.user.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.Optional;


/**
 * Main View controller.
 */
public final class LoginViewController {

    @FXML private TextField loginUsername;
    @FXML private PasswordField loginPassword;
    @FXML private Button loginButton;
    @FXML private Button registerButton;
    private final User user = User.getUser();
    private final UserQuery userQuery = new UserQuery();

    @FXML public void handleSubmitRegistration(final ActionEvent event) throws Exception {
        Parent register = FXMLLoader.load(getClass().getResource("/fxml/connection/RegisterView.fxml"));
        Scene scene = new Scene(register, 640, 720);
        Stage stage = (Stage) loginUsername.getScene().getWindow();
        stage.setScene(scene);
    }

    @FXML public void handleSubmitLogin(final ActionEvent event) {
        final Optional<String> userName = checkCredential();
        if (!userName.isPresent()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeight(500);
            alert.setWidth(700);
            alert.setResizable(false);
            //alert.setTitle("Login Fail");
            alert.setHeaderText("Wrong credential");
            alert.setContentText("Username or Password are wrong, retry");
            alert.showAndWait();
            return;
        }
        try {
            user.setName(loginUsername.getText());
            user.setPassword(loginPassword.getText());
            Parent changeToMainView = FXMLLoader.load(getClass().getResource("/fxml/MainView.fxml"));
            Scene mainScene = new Scene(changeToMainView, 1280, 720);
            Stage mainStage = (Stage) loginPassword.getScene().getWindow();
            mainStage.setScene(mainScene);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Optional<String> checkCredential() {
        return userQuery.getUserByLogin(loginUsername.getText(), loginPassword.getText());
    }
}
