package it.unibo.db.ui.connection;

import it.unibo.db.query.UserQuery;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class RegisterViewController {

    @FXML private TextField registerUsername;
    @FXML private PasswordField registerPassword;
    @FXML private TextField registerName;
    @FXML private TextField registerSurname;
    @FXML private TextField registerEmail;
    @FXML private TextField registerStreet;
    @FXML private TextField registerNumber;
    @FXML private TextField registerCap;
    @FXML private TextField registerCity;
    @FXML private TextField registerState;
    @FXML private Button registerSubmit;
    private final UserQuery userQuery = new UserQuery();

    @FXML private void registerSubmitAction() {

        if (checkEmptyFields()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Error");
            alert.setContentText("You must specify all fields");
            alert.showAndWait();
        } else {
            userQuery.insertNewUser(registerUsername.getText(), registerPassword.getText(),
                    registerName.getText(), registerSurname.getText(), registerEmail.getText(),
                    registerStreet.getText(), Integer.parseInt(registerNumber.getText()),
                    Integer.parseInt(registerCap.getText()), registerCity.getText(), registerState.getText());
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Success");
            alert.setContentText("The user: " + registerUsername.getText() + " was successfully registered");
            alert.showAndWait();
        }

    }

    @FXML private void goBackToLogin() throws Exception {
        Parent register = FXMLLoader.load(getClass().getResource("/fxml/connection/LoginView.fxml"));
        Scene scene = new Scene(register, 640, 720);
        Stage stage = (Stage) registerCap.getScene().getWindow();
        stage.setScene(scene);
    }

    private boolean checkEmptyFields() {
        return registerUsername.getText().trim().isEmpty()
                || registerPassword.getText().trim().isEmpty()
                || registerName.getText().trim().isEmpty()
                || registerSurname.getText().trim().isEmpty()
                || registerEmail.getText().trim().isEmpty()
                || registerStreet.getText().trim().isEmpty()
                || registerNumber.getText().trim().isEmpty()
                || registerCap.getText().trim().isEmpty();
    }
}
