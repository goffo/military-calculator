package it.unibo.db.user;

public class User {
    private String name;
    private String password;
    private static final User USER = new User();

    private User() {

    }

    public static User getUser() {
        return USER;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
