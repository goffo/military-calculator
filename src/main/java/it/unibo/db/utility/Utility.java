package it.unibo.db.utility;

import it.unibo.db.connector.ConnectionSingleton;
import it.unibo.db.connector.ConnectorDB;
import it.unibo.db.weapon.Material;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;

import static it.unibo.db.database.tables.Material.MATERIAL;

public class Utility {

    public static Integer materialNameToCode(Material material) {
        final ConnectorDB connectorDB = ConnectionSingleton.getConnector();
        final Connection connection = connectorDB.getConnection();
        DSLContext qry = DSL.using(connection, SQLDialect.MYSQL);
        Result<Record1<Integer>> res = qry.select(MATERIAL.CODM)
                .from(MATERIAL)
                .where(MATERIAL.NAME.eq(material.getMaterialName()))
                .fetch();
        return res.get(0).value1();
    }

    public static Integer materialNameToCode(String material) {
        final ConnectorDB connectorDB = ConnectionSingleton.getConnector();
        final Connection connection = connectorDB.getConnection();
        DSLContext qry = DSL.using(connection, SQLDialect.MYSQL);
        Result<Record1<Integer>> res = qry.select(MATERIAL.CODM)
                .from(MATERIAL)
                .where(MATERIAL.NAME.eq(material))
                .fetch();
        return res.get(0).value1();
    }

    public static String materialCodeToName(Integer material) {
        final ConnectorDB connectorDB = ConnectionSingleton.getConnector();
        final Connection connection = connectorDB.getConnection();
        DSLContext qry = DSL.using(connection, SQLDialect.MYSQL);
        Result<Record1<String>> res = qry.select(MATERIAL.NAME)
                .from(MATERIAL)
                .where(MATERIAL.CODM.eq(material))
                .fetch();
        return res.get(0).value1();
    }
}
