package it.unibo.db.weapon;

/**
 * Modelling a material with it's price.
 */
public interface Material {
    /**
     *
     * @return material's price.
     */
    Double getPrice();
    void setPrice(Double price);

    /**
     * .
     * @return the quantity.
     */
    Integer getQuantity();
    void setQuantity(Integer quantity);
    void addQuantity(Integer value);

    /**
     * .
     * @return Material name.
     */
    String getMaterialName();
    void setMaterialName(String materialName);
}
