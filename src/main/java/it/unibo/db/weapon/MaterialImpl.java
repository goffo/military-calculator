package it.unibo.db.weapon;

import javafx.beans.property.*;

import java.math.BigDecimal;
import java.util.Objects;

public class MaterialImpl implements Material {
    private final DoubleProperty price;
    private final StringProperty materialName;
    private final IntegerProperty quantity;

    public MaterialImpl(final String materialName, final BigDecimal price, final BigDecimal quantity) {
        this.price = new SimpleDoubleProperty(price.doubleValue());
        this.materialName = new SimpleStringProperty(materialName);
        this.quantity = new SimpleIntegerProperty(quantity.intValue());
    }

    @Override
    public Double getPrice() {
        return price.get();
    }

    @Override
    public String getMaterialName() {
        return this.materialName.get();
    }

    @Override
    public Integer getQuantity() {
        return this.quantity.get();
    }

    @Override
    public String toString() {
        return "MaterialImpl{" +
                "price=" + price +
                ", materialName=" + materialName +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public void setPrice(Double price) {
        this.price.setValue(price);
    }

    @Override
    public void setQuantity(Integer quantity) {
        this.quantity.setValue(quantity);
    }

    @Override
    public void setMaterialName(String materialName) {
        this.materialName.setValue(materialName);
    }

    @Override
    public void addQuantity(Integer value) {
        this.quantity.setValue(this.quantity.doubleValue() + value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MaterialImpl material = (MaterialImpl) o;
        return Objects.equals(price, material.price) &&
                Objects.equals(materialName, material.materialName) &&
                Objects.equals(quantity, material.quantity);
    }

    @Override
    public int hashCode() {

        return Objects.hash(price, materialName, quantity);
    }
}
