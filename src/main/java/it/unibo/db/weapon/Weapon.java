package it.unibo.db.weapon;

public interface Weapon {

    String getWeaponName();
    void setWeaponName(String weaponName);
    String getWeaponCategory();
    void setWeaponCategory(String weaponCategory);
    String getWeaponImage();
    void setWeaponImage(String weaponImage);

}
