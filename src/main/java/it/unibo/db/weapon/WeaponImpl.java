package it.unibo.db.weapon;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class WeaponImpl implements Weapon {

    private StringProperty weaponName;
    private StringProperty weaponCategory;
    private StringProperty weaponImage;

    public WeaponImpl(final String weaponName, final String weaponCategory, final String weaponImage) {
        this(weaponName, weaponCategory);
        this.weaponImage = new SimpleStringProperty(weaponImage);
    }

    public WeaponImpl(final String weaponName, final String weaponCategory) {
        this.weaponName = new SimpleStringProperty(weaponName);
        this.weaponCategory = new SimpleStringProperty(weaponCategory);
        this.weaponImage = null;
    }

    public WeaponImpl(final String weaponName) {
        this.weaponName = new SimpleStringProperty(weaponName);
        this.weaponImage = null;
        this.weaponCategory = null;
    }

    @Override
    public String getWeaponName() {
        return this.weaponName.getValue();
    }

    @Override
    public void setWeaponName(String weaponName) {
        this.weaponName.setValue(weaponName);
    }

    @Override
    public String getWeaponCategory() {
        return this.weaponCategory.getValue();
    }

    @Override
    public void setWeaponCategory(String weaponCategory) {
        this.weaponCategory.setValue(weaponCategory);
    }

    @Override
    public String getWeaponImage() {
        return weaponImage.getValue();
    }

    @Override
    public void setWeaponImage(String weaponImage) {
        if (this.weaponImage == null) {
            this.weaponImage = new SimpleStringProperty(weaponImage);
        } else {
            this.weaponImage.setValue(weaponImage);
        }
    }
}
