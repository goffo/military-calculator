package it.unibo.db.weapon;

import it.unibo.db.connector.ConnectionSingleton;
import it.unibo.db.connector.ConnectorDB;
import it.unibo.db.query.WeaponQuery;
import it.unibo.db.user.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import org.jooq.*;
import org.jooq.impl.DSL;

import javax.jws.soap.SOAPBinding;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.*;
import java.util.stream.Collectors;

import static it.unibo.db.database.tables.Material.MATERIAL;
import static it.unibo.db.database.tables.Ingredient.INGREDIENT;
import static it.unibo.db.database.tables.Component.COMPONENT;
import static it.unibo.db.database.tables.Weapon.WEAPON;
import static it.unibo.db.database.tables.Skill.SKILL;
import static it.unibo.db.database.tables.Requirementw.REQUIREMENTW;
import static it.unibo.db.database.tables.Association.ASSOCIATION;
import static  it.unibo.db.database.tables.Statistic.STATISTIC;
import static org.jooq.impl.DSL.sum;

public class WeaponManager extends Alert {

    private ButtonType addMaterial = new ButtonType("Add material");
    private ButtonType craft = new ButtonType("Craft");
    private ButtonType cancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
    private String weaponName;
    private Result<Record3<String, BigDecimal, BigDecimal>> queryResult;
    private ObservableList<String> abilityList;
    private ObservableList<String> statisticList;
    private ObservableList<Material> materialObservableList;
    private WeaponQuery weaponQuery = new WeaponQuery();
    private User user = User.getUser();

    public WeaponManager(final String weaponName) {
        super(AlertType.NONE);
        super.setResizable(true);
        this.weaponName = weaponName;
        queryResult = materialQuery();

        BorderPane borderPane = new BorderPane();
        Label title = new Label("Material");
        title.setFont(new Font(24));
        borderPane.setTop(title);
        ListView<String> ability = new ListView<>();
        ListView<String> statistic = new ListView<>();
        abilityList = FXCollections.observableArrayList(getAbility(weaponName));
        statisticList = FXCollections.observableArrayList(getStatistic(weaponName));
        VBox vBox = new VBox();
        vBox.setSpacing(25);
        ability.setItems(abilityList);
        statistic.setItems(statisticList);
        vBox.getChildren().addAll(new Label("Abilities"), ability, new Label("Statistics"), statistic);

        TableView<Material> tableView = new TableView<>();
        TableColumn<Material, String> materialName = new TableColumn<>("Material Name");
        TableColumn<Material, Integer> materialQuantity = new TableColumn<>("Quantity");
        TableColumn<Material, Double> materialPrice = new TableColumn<>("Price");
        materialObservableList = FXCollections.observableArrayList();
        VBox table = new VBox();
        table.setSpacing(25);
        table.getChildren().addAll(new Label("Material"), tableView);
        borderPane.setCenter(table);
        borderPane.setLeft(vBox);

        for (Record3<String, BigDecimal, BigDecimal> res : queryResult) {
            materialObservableList.add(new MaterialImpl(res.component1(), res.component3(), res.component2()));
        }
        materialName.setCellValueFactory(new PropertyValueFactory<>("materialName"));
        materialQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        materialPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        tableView.setItems(materialObservableList);
        tableView.getColumns().add(materialName);
        tableView.getColumns().add(materialPrice);
        tableView.getColumns().add(materialQuantity);

        super.getDialogPane().setContent(borderPane);
        super.getButtonTypes().addAll(addMaterial, craft, cancel);

        if (!weaponQuery.checkAbilities(user.getName(), weaponName) || !weaponQuery.checkMaterial(user.getName(), weaponName)) {
            super.getDialogPane().lookupButton(craft).setDisable(true);
        }
    }

    private WeaponManager(final AlertType alertType, final String contentText, final ButtonType... buttons) {
        super(alertType, contentText, buttons);
    }

    public List<Material> showAndConfirm() {
        final Optional<ButtonType> result = super.showAndWait();
        if (result.get() == addMaterial) {
            System.out.println("Buy");
            if (queryResult.isEmpty()) {
                return Collections.emptyList();
            }
            return queryResult.stream()
                    .map(res -> new MaterialImpl(res.component1(), res.component3(), res.component2()))
                    .collect(Collectors.toList());
        } else if (result.get() == craft) {
            System.out.println("Craft");
            weaponQuery.craftWeapon(user.getName(), materialObservableList);
            weaponQuery.insertWeapon(user.getName(), weaponName);

        } else {
            System.out.println("none");
        }
        return null;
    }

    private Result<Record3<String, BigDecimal, BigDecimal>> materialQuery() {
        final ConnectorDB connectorDB = ConnectionSingleton.getConnector();
        final Connection connection = connectorDB.getConnection();
        DSLContext query = DSL.using(connection, SQLDialect.MYSQL);

        /*
         * select Material.name, sum(Ingredient.quantity) as qty, sum(Material.price) as price
         * from Material join Ingredient on Material.codM = Ingredient.material
         * join Component on Ingredient.component = Component.codComp
         * join Weapon on Component.weapon = Weapon.codW
         * where Weapon.name = param
         * group by Material.name
         */
        return query.select(MATERIAL.NAME, sum(INGREDIENT.QUANTITY), sum(MATERIAL.PRICE))
                .from(MATERIAL)
                .join(INGREDIENT).on(MATERIAL.CODM.eq(INGREDIENT.MATERIAL))
                .join(COMPONENT).on(INGREDIENT.COMPONENT.eq(COMPONENT.CODCOMP))
                .join(WEAPON).on(COMPONENT.WEAPON.eq(WEAPON.CODW))
                .where(WEAPON.NAME.eq(weaponName))
                .groupBy(MATERIAL.NAME)
                .fetch();
    }

    private List<String> getAbility(final String weaponName) {
        final ConnectorDB connectorDB = ConnectionSingleton.getConnector();
        final Connection connection = connectorDB.getConnection();
        DSLContext query = DSL.using(connection, SQLDialect.MYSQL);
        Result<Record2<String, Integer>> res = query.select(SKILL.NAME, REQUIREMENTW.LEVEL)
                .from(SKILL)
                .join(REQUIREMENTW)
                .on(SKILL.CODS.eq(REQUIREMENTW.SKILL))
                .join(WEAPON)
                .on(REQUIREMENTW.WEAPON.eq(WEAPON.CODW))
                .where(WEAPON.NAME.eq(weaponName))
                .fetch();
        return res.stream()
                .map(e -> e.value1() + ": " + e.value2())
                .collect(Collectors.toList());
    }

    private List<String> getStatistic(final String weaponName) {
        final ConnectorDB connectorDB = ConnectionSingleton.getConnector();
        final Connection connection = connectorDB.getConnection();
        DSLContext query = DSL.using(connection, SQLDialect.MYSQL);

        Result<Record2<String, Integer>> res = query.select(STATISTIC.NAME, ASSOCIATION.LEVEL)
                .from(WEAPON)
                .join(ASSOCIATION)
                .on(WEAPON.CODW.eq(ASSOCIATION.WEAPON))
                .join(STATISTIC)
                .on(ASSOCIATION.STATISTIC.eq(STATISTIC.CODS))
                .where(WEAPON.NAME.eq(weaponName))
                .fetch();
        return res.stream()
                .map(e -> e.component1() + ": " + e.component2())
                .collect(Collectors.toList());
    }
}
